﻿using System;
using System.Data;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
namespace Repository
{
    public class AdoContext : IDisposable
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private string connectionString = "server=mydbinstance.c6vpabv2tl9d.ap-south-1.rds.amazonaws.com;port=3306;uid=novartis;pwd=password;database=image_hunter";
        //private const string connectionString = "server = 10.207.0.12; database = unicorn2; user = root; password = nopassword;";


        public AdoContext()
        {
            _connection = new MySqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }
        public IDbCommand CreateTransactionCommand()
        {
            var command = _connection.CreateCommand();
            command.Transaction = _transaction;
            return command;
        }

        public void ExecuteTransactionlCommand(Action<IDbCommand> action)
        {
            using (var command = _connection.CreateCommand())
            {
                using (var transaction = _connection.BeginTransaction())
                {
                    command.Transaction = transaction;
                    action(command);
                    transaction.Commit();
                }
            }
        }

        public void ExecuteCommand(Action<IDbCommand> action)
        {
            using (var command = _connection.CreateCommand())
            {
                action(command);
            }
        }

        public void SaveChanges()
        {
            if (_transaction == null)
            {
                throw new InvalidOperationException("Transaction have already been already been commited. Check your transaction handling.");
            }
            _transaction.Commit();
            _transaction = null;
        }

        public void Dispose()
        {
            if (_transaction != null)
            {
                _transaction.Commit();
                //_transaction.Rollback();
                _transaction = null;
            }
            if (_connection != null)
            {
                _connection.Close();
                _connection = null;
            }
        }
    }
}
