﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using models;

namespace Repository
{
    public class GameRepository
    {
        public string ItemName { get; set; }
        public int itemLevel { get; set; }
        public string inputTrainingData { get; set; }
        public GameItemModel MapGameItem(IDataReader reader)
        {
            GameItemModel model = new GameItemModel();
            for (var i = 0; i < reader.FieldCount; i++)
            {
                var fieldName = reader.GetName(i);
                var fieldValue = reader.GetValue(i);
                if (fieldValue != null)
                {
                    switch (fieldName)
                    {
                        case "gameitem":
                            model.ItemName = Convert.ToString(fieldValue);
                            break;
                        case "level":
                            model.itemLevel = Convert.ToInt32(fieldValue);
                            break;
                        case "trainingdata":
                            model.inputTrainingData = Convert.ToString(fieldValue);
                            break;
                        case "gameitemid":
                            model.ItemId = Convert.ToInt32(fieldValue);
                            break;
                        default:
                            break;
                    }
                }
            }
            return model;
        }
        public List<GameItemModel> GetAllGameItems()
        {
            List<GameItemModel> gameItems = new List<GameItemModel>();
            try
            {
                using (var context = new AdoContext())
                {
                    var command = context.CreateTransactionCommand();
                    string query = "Select * from `gameitems` WHERE isactive = true";
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            gameItems.Add(MapGameItem(reader));
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return gameItems;
        }

        


        public Boolean AddItem(string itemName, int itemLevel, string inputTrainingData, bool isActive)
        {
            try
            {
                using (var context = new AdoContext())
                {
                    var command = context.CreateTransactionCommand();
                    string query = "INSERT into `gameitems` (`gameitem`,`level`,`trainingdata`,`isactive`) Values (?gameItem, ?level, ?trainingData , ?isactive)";
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;
                    MySqlParameter gameItem = new MySqlParameter("?gameItem", MySqlDbType.VarChar);
                    gameItem.Value = itemName.ToString();
                    command.Parameters.Add(gameItem);
                    MySqlParameter level = new MySqlParameter("?level", MySqlDbType.Int32);
                    level.Value = itemLevel;
                    command.Parameters.Add(level);
                    MySqlParameter trainingData = new MySqlParameter("?trainingData", MySqlDbType.Text);
                    trainingData.Value = inputTrainingData.ToString();
                    command.Parameters.Add(trainingData);
                    MySqlParameter isactive = new MySqlParameter("?isactive", MySqlDbType.Int16);
                    isactive.Value = 1;
                    command.Parameters.Add(isactive);
                    using (var reader = command.ExecuteReader())
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message, ex.InnerException);
                return false;
            }
        }
        public GameItemModel GetGameItemByName(string name)
        {
            var gameItems = new GameItemModel();
            try
            {
                using (var context = new AdoContext())
                {
                    var command = context.CreateTransactionCommand();
                    string query = "Select * from `gameitems` WHERE gameitem = ?name";
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;
                    var nameParam = new MySqlParameter("?name", MySqlDbType.VarChar);
                    nameParam.Value = name;
                    command.Parameters.Add(nameParam);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return MapGameItem(reader);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return gameItems;
        }
        public Boolean AddImage(string itemName, List<string> images)
        {
            bool successful = false;
            try
            {
                using (var context = new AdoContext())
                {

                    var currentItem = GetGameItemByName(itemName);
                    var gameItemId = currentItem.ItemId;
                    string query = "INSERT into `gameitemimage` (`gameitemid`,`image`, `isactive`) Values (?gameitemid, ?image, ?isactive)";
                    for (int i = 0; i < images.Count; i++)
                    {
                        var command = context.CreateTransactionCommand();
                        command.CommandText = query;
                        command.CommandType = CommandType.Text;
                        MySqlParameter gameItem = new MySqlParameter("?gameitemid", MySqlDbType.Int32);
                        gameItem.Value = gameItemId;
                        command.Parameters.Add(gameItem);
                        MySqlParameter image = new MySqlParameter("?image", MySqlDbType.LongText);
                        image.Value = images[i];
                        command.Parameters.Add(image);
                        MySqlParameter isactive = new MySqlParameter("?isactive", MySqlDbType.Int16);
                        isactive.Value = 1;
                        command.Parameters.Add(isactive);

                        using (var reader = command.ExecuteReader())
                        {
                            successful = true;
                        }
                    }
                    return successful;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message, ex.InnerException);
                return successful;
            }
        }
        
        public List<ImageDataModel> GetAllImageItems()
        {
            var gameImageItems = new List<ImageDataModel>();
            var gameItems = GetAllGameItems();
            string query = "Select image from `gameitemimage` WHERE isactive = true and gameitemid = ?itemId";
            foreach (var gameItem in gameItems)
            {
                try
                {
                    var imageItem = new ImageDataModel
                    {
                        ItemName = gameItem.ItemName,
                        ItemId = gameItem.ItemId,
                        Images = new List<string>()
                    };

                    using (var context = new AdoContext())
                    {
                        var command = context.CreateTransactionCommand();
                        command.CommandText = query;
                        command.CommandType = CommandType.Text;

                        MySqlParameter gameItemParam = new MySqlParameter("?itemId", MySqlDbType.Int32);
                        gameItemParam.Value = gameItem.ItemId;
                        command.Parameters.Add(gameItemParam);

                        using (var reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                 imageItem.Images.Add(Convert.ToString(reader["image"]));
                            }
                        }
                    }
                    gameImageItems.Add(imageItem);
                }
                catch (Exception ex)
                {

                }
            }
            return gameImageItems;
        }

    }
}