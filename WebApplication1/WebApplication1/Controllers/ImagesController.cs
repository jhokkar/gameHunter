﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using models;
using Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class ImagesController : Controller
    {
        [HttpGet]
        [EnableCors("MyPolicy")]
        public IEnumerable<ImageDataModel> Get()
        {
            GameRepository gameRepo = new GameRepository();
            var items = gameRepo.GetAllImageItems();
            return items;
        }

        // POST api/values

        [HttpPost]
        [EnableCors("MyPolicy")]
        public bool Post([FromBody] ImageDataModel itemData)
        {
            GameRepository gameRepo = new GameRepository();
            bool isItemAdded = gameRepo.AddImage(itemData.ItemName, itemData.Images);
            return isItemAdded;
        }
    }
}
