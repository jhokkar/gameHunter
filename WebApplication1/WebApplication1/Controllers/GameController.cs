﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Repository;
using models;
using Microsoft.AspNetCore.Cors;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class GameController : Controller
    {

        // GET api/values/
        [HttpGet] [EnableCors("MyPolicy")]
        public IEnumerable<GameItemModel> Get()
        {
            GameRepository gameRepo = new GameRepository();
            List<GameItemModel> items = new List<GameItemModel>();
            items = gameRepo.GetAllGameItems();
            return items;
        }

        // POST api/values

        [HttpPost] [EnableCors("MyPolicy")] 
        public void Post([FromBody] GameItemModel itemData)
        {
            GameRepository gameRepo = new GameRepository();
            bool isItemAdded = gameRepo.AddItem(itemData.ItemName, itemData.itemLevel, itemData.inputTrainingData , itemData.isActive);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
