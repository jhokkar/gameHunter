import React from 'react';
import PropTypes from 'prop-types';

import CheckBoxComponent from 'components/checkbox';

export default class DataSourcesColumnListComponent extends React.PureComponent {

  render() {
    const { dataSourceColumns, selectedDataSource } = this.props;
    console.log('DataSourcesColumnListComponent', this.props);

    return (
      <div >
        {selectedDataSource ?
          (
            <div className="row d-flex no-gutters">
              <div className="flex-grow-1 px-2">
                <span className="fa-stack fa-1x">
                  <i className="fa text-theme-color fa-dollar fa-stack-1x"></i>
                  <i className="fa-stack-1x fa fa-circle fa-circle-round"></i>
                </span>
                <span className="ml-2"> {selectedDataSource.DataSourceName}
                </span>
              </div>
            </div>
          )
          : ''}
        <div className="row  no-gutters d-flex smart-frame-height smart-frame-variables border-all-1 shadow">
          <div className="col-12">
            {dataSourceColumns.length === 0 ?
              <div className="d-block text-center text-muted ml-auto mr-auto p-5" >
                <div><i className="fa fa-3x fa-close"></i></div>
                <div>No Columns found</div>
              </div> : ''
            }
            <ul className="list-inline variable-check-box-list">
              {dataSourceColumns.map((column) => (
                <li key={column.value} className="list-inline-item">
                  <div className="variable-check-box shadow-sm">
                    <CheckBoxComponent
                      label={column.name}
                      labelType={column.type}
                      checked={column.status}
                      disabled={false}
                      emitChange={(value) => this.props.onVariableSection({ value, column, selectedDataSource })}
                    />
                  </div>
                </li>
              ))
              }
            </ul>
          </div>
        </div>
      </div>

    );
  }
}

DataSourcesColumnListComponent.propTypes = {
  loading: PropTypes.bool,
  dataSourceColumns: PropTypes.array,
  selectedDataSource: PropTypes.object,
  onVariableSection: PropTypes.func
};
