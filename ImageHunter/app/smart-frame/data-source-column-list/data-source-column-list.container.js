import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import DataSourcesColumnListComponent from './data-source-column-list.component';
import { getDataSourceColumns, getSelectedDataSource } from '../smart-frame.selector';
import { onVariableSection } from '../smart-frame.actions';

const mapDispatchToProps = (dispatch) => ({
  onVariableSection: (variableObject) => dispatch(onVariableSection(variableObject))
});

const mapStateToProps = createStructuredSelector({
  dataSourceColumns: getDataSourceColumns(),
  selectedDataSource: getSelectedDataSource()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const DataSourcesColumnListContainer = compose(withConnect)(DataSourcesColumnListComponent);

export default DataSourcesColumnListContainer;
