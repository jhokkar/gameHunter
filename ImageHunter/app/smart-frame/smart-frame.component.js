import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Modal } from 'reactstrap';
import { Link } from 'react-router-dom';
import './smart-frame.scss';
import DataSourcesColumnListContainer from './data-source-column-list';
import TooltipComponent from '../components/tool-tip/tool-tip.component';
import ReactDrawer from 'react-drawer';
import DataSourcesListContainer from './data-sources-list';
import PublishSmartFrameComponent from 'components/publish-smart-frame/publish-modal-popup.component';


export default class SmartFrameComponent extends React.PureComponent {

  constructor(props) {
    super(props);
    // Internal actions, No need of Redux Store usage
    this.state = {
      isPaneOpen: false
    };
  }
  state = {
    openPublishSmartFrame: false,
  };

  onOpenPublishSmartFrameModal = () => {
    this.props.onPublishSmartFrameClick();
    this.setState({ openPublishSmartFrame: true });
  };

  onClosePublishSmartFrameModal = () => {
    this.setState({ openPublishSmartFrame: false });
  };

  render() {
    console.log('smartframe', this.props);
    return (
      <section>
        <Helmet>
          <title>Edit Smart Frame | SwitchBoardX</title>
          <meta name="description" content="Home, SwitchBoardX" />
        </Helmet>
        <div className="smart-frame-page">
          <section>
            <div className="row shadow-sm  d-flex-inline justify-content-between">
              <div className="px-2 d-inline-flex">
                <div >
                  <Link to="/dashboard">
                    <i className="fa fa-2x half-opacity fa-chevron-circle-left"></i>
                  </Link>
                </div>
                <div className="py-1 ml-1">
                  <h5>Edit Recommended Smart Frame</h5>
                </div>
              </div>
              <div className="px-2 py-1">
                <input
                  type="button"
                  value="PUBLISH SMART FRAME"
                  className="btn btn-primary btn-font-size"
                  onClick={this.onOpenPublishSmartFrameModal}
                />
              </div>
              <Modal isOpen={this.state.openPublishSmartFrame} toggle={this.onClosePublishSmartFrameModal}>
                <PublishSmartFrameComponent toggle={this.onClosePublishSmartFrameModal} data={this.props.smartFrameDataDetails} />
              </Modal>
            </div>
            <div className="row mt-2 mx-1">
              <div className="col-md-3 data-sources-sidebar border-top shadow d-none d-md-block">
                <DataSourcesListContainer />
              </div>
              <div className="col-md-9 col-sm-12">
                <div className="d-md-none d-inline react-drawer-scroll">
                  <a className=" mt-2" id="dataSourcesList" href="javascript:void(0)" onClick={() => this.setState({ isPaneOpen: true })}>
                    <i className="fa fa-1x fa-align-justify"></i>
                  </a>
                  <TooltipComponent
                    placement="top"
                    targetId="dataSourcesList"
                    text="Data Sources"
                  />
                  <ReactDrawer
                    open={this.state.isPaneOpen}
                    position="left"
                    onClose={() => {
                      // triggered on "<" on left top click or on outside click
                      this.setState({ isPaneOpen: false });
                    }}
                  >
                    <div className="px-2" >
                      <DataSourcesListContainer />
                    </div>

                  </ReactDrawer>
                </div>
                <DataSourcesColumnListContainer />
              </div>
            </div>
          </section>
        </div>
      </section >
    );
  }
}

SmartFrameComponent.propTypes = {
  loading: PropTypes.bool,
  smartFrameDataDetails: PropTypes.object,
  onPublishSmartFrameClick: PropTypes.func,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),
};
