import { ON_DATA_SOURCE_SELECTION, ON_VARIABLE_SELECTION, BIND_PUBLISH_SMART_FRAME_POPUP } from 'constants';
import { getSmartFrameDetails } from './smart-frame.service';

export function onDataSourceSelection(selectedDataSource) {
  return (dispatch) => {
    dispatch({
      type: ON_DATA_SOURCE_SELECTION,
      payload: selectedDataSource
    }
    );
  };
}
export function onVariableSection(variableObject) {
  return (dispatch) => {
    dispatch({
      type: ON_VARIABLE_SELECTION,
      payload: variableObject
    }
    );
  };
}
export function onPublishSmartFrameClick() {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());

    getSmartFrameDetails().then((resp) => {
      console.log(resp);
      dispatch({
        type: BIND_PUBLISH_SMART_FRAME_POPUP,
        payload: resp.data
      }
      );
      dispatch(getState().spinnerStore.EndTask());

    }).catch(() => {
      dispatch(getState().spinnerStore.EndTask());
    })

  };
}

