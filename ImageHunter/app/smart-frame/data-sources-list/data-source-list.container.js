import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import DataSourcesListComponent from './data-source-list.component';
import { getDistinctSelectedDataSets } from '../../dashboard/dashboard.selector';
import { onDataSourceSelection } from '../smart-frame.actions';
import { getSelectedDataSource } from '../smart-frame.selector';

const mapDispatchToProps = (dispatch) => ({
  onDataSourceSelection: (dataSource) => dispatch(onDataSourceSelection(dataSource))
});

const mapStateToProps = createStructuredSelector({
  selectedDataSets: getDistinctSelectedDataSets(),
  selectedDataSource: getSelectedDataSource()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const DataSourcesListContainer = compose(withConnect)(DataSourcesListComponent);

export default DataSourcesListContainer;
