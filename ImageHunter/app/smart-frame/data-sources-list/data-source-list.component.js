import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class DataSourceListComponent extends React.PureComponent {

  render() {
    const { selectedDataSets, selectedDataSource } = this.props;
    console.log('DataSourceListComponent', this.props);
    return (
      <div className="">
        <div className="d-flex ">
          <div className="p-2 flex-grow-1 ">Data Sources</div>
        </div>
        <div className="row no-gutters">
          <div className="col-12">
            {selectedDataSets.length === 0 ?
              <div className="d-block text-center text-muted mb-2">
                <div><i className="fa fa-3x fa-close"></i></div>
                <div>No Records found</div>
              </div> : ''
            }
            <ul className="data-source-list smart-frame-height text-normal-font-size list-group list-group-flush">
              {selectedDataSets.map((dataSource) => {
                const activeClassName = `list-group-item justify-content-between align-items-center ${dataSource.ID == selectedDataSource.ID ? ' active' : ''}`;
                return (
                  <li
                    key={dataSource.ID}
                    className={activeClassName}
                  >
                    <div className="d-flex ">
                      <div className="py-2 px-1 ">
                        <i className="fa fa-spotify"></i>
                      </div>
                      <div className="p-2 flex-grow-1">
                        <a href="javascript:void(0)" onClick={() => { this.props.onDataSourceSelection(dataSource); }}> {dataSource.DataSourceName}</a>
                      </div>
                      {/* <div className="p-2 ml-auto">
                        <span className="badge badge-primary badge-pill">xx%</span>
                      </div> */}
                    </div>
                  </li>
                )
              })}

            </ul>
            <ul className="list-group list-group-flush border-top-shadow-2">
              <li className="list-group-item text-center text-uppercase">
                <Link to="/dashboard">
                  <span className="find-more"> Find More DataSources</span>
                </Link>
              </li>
            </ul>

          </div>
        </div>
      </div>

    );
  }
}

DataSourceListComponent.propTypes = {
  loading: PropTypes.bool,
  selectedDataSource: PropTypes.object,
  selectedDataSets: PropTypes.array,
  onDataSourceSelection: PropTypes.func
};
