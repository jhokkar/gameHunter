import {
  ADD_UPDATE_SELECTED_COLUMNS,
  ON_DATA_SOURCE_SELECTION,
  ON_VARIABLE_SELECTION,
  BIND_PUBLISH_SMART_FRAME_POPUP
} from 'constants';

/*
 * SmartFrameReducer
 *
 */

// The initial state of the SmartFrame
const initialState = ({
  selectedDataSources: [],
  selectedDataColumns: [
    { name: 'Zip Code', value: 'zipCode', status: false },
    { name: 'City', value: 'city', status: true },
    { name: 'Test Rv', value: 'testRv', type: 'RV', status: true },
    { name: 'Date/day of the week', value: 'date', status: false }
  ],
  smartFrameDataDetails: {
    FileSize: 0,
    FilePath: '',
    Frames: 0,
    Rows: 0
  }
});

function smartFrameReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_UPDATE_SELECTED_COLUMNS: {
      return { ...state, selectedDataColumns: action.searchInput };
    }
    case ON_DATA_SOURCE_SELECTION: {
      const dataColumns = [
        { name: 'Zip Code', value: 'zipCode', status: false },
        { name: 'City', value: 'city', status: true },
        { name: 'Test Rv', value: 'testRv', type: 'RV', status: true },
        { name: 'Date/day of the week', value: 'date', status: false }
      ];
      const selectedDataSource = action.payload;
      return {
        ...state,
        selectedDataSourceId: selectedDataSource.ID,
        selectedDataColumns: dataColumns
      };
    }
    case BIND_PUBLISH_SMART_FRAME_POPUP: {
      const data = action.payload;
      return { ...state, smartFrameDataDetails: { ...data } };
    }
    case ON_VARIABLE_SELECTION: {
      return { ...state };
    }
    default:
      return state;
  }
}

export default smartFrameReducer;
