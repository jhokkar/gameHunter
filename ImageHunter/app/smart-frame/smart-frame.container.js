import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import {
  makeSelectLoading,
  makeSelectError
} from '../layout/layout.selectors';
import reducer from './smart-frame.reducer';
import SmartFrameComponent from './smart-frame.component';
import { onDataSourceSelection, onPublishSmartFrameClick } from './smart-frame.actions';
import { getSmartFrameDataDetails } from './smart-frame.selector';

const mapDispatchToProps = (dispatch) => ({
  onDataSourceClick: (dataSource) => dispatch(onDataSourceSelection(dataSource)),
  onPublishSmartFrameClick: () => dispatch(onPublishSmartFrameClick())
});

const mapStateToProps = createStructuredSelector({
  loading: makeSelectLoading(),
  error: makeSelectError(),
  smartFrameDataDetails: getSmartFrameDataDetails()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'smartFrame', reducer });
export { mapDispatchToProps };
const SmartFrameContainer = compose(withReducer, withConnect)(SmartFrameComponent);
export default SmartFrameContainer;
