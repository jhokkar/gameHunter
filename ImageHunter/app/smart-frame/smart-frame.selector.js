/**
 * Smart Frame selectors
 */

import { createSelector } from 'reselect';
import _ from 'lodash';
import { getSelectedDataItems } from '../dashboard/dashboard.selector';

const selectSmartFrame = (state) => state.smartFrame;
const selectedDataSourceIdSelector = (state) => state.smartFrame.selectedDataSourceId;
const dataColumnSelector = (state) => state.smartFrame.selectedDataColumns;
const smartFrameDetailsSelector = (state) => state.smartFrame.smartFrameDataDetails;

const getSelectedDataSource = () => createSelector(
  selectedDataSourceIdSelector,
  getSelectedDataItems(),
  (selectedDataSourceId, selectedDataItems) => {
    let item = {};
    if (selectedDataSourceId) {
      item = _.find(selectedDataItems, (selectedDataItem) => selectedDataItem.ID == selectedDataSourceId);
    } else {
      item = selectedDataItems[0];
    }
    return item;
  }
);

const getDataSourceColumns = () => createSelector(
  dataColumnSelector,
  (dataColumns) => {
    const items = dataColumns ? dataColumns.map((selectedColumn) => ({ ...selectedColumn })) : [];
    return items;
  }
);

const getSmartFrameDataDetails = () => createSelector(
  smartFrameDetailsSelector,
  (smartFrameDetails) => {
    return { ...smartFrameDetails };
  }
);

export {
  selectSmartFrame,
  getSelectedDataSource,
  getDataSourceColumns,
  getSmartFrameDataDetails
};
