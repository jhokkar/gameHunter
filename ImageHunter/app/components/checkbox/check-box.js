/**
 * Checkbox
 * ref:  https://codepen.io/devhamsters/pen/JEPgop
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

class CheckBoxComponent extends PureComponent {
  static propTypes = {
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    emitChange: PropTypes.func,
    label: PropTypes.string
  };
  static defaultProps = {
    checked: false,
    disabled: false,
    label: ''
  };
  constructor(props) {
    super(props);
    this.state = {
      checked: props.checked,
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.checked !== this.props.checked) {
      this.setState({
        checked: nextProps.checked
      });
    }
  }

  handleCheckBoxChange = () => {
    this.setState({
      checked: !this.state.checked,
    });
    this.props.emitChange(!this.state.checked);
  };

  render() {
    const { disabled } = this.props;
    const { checked } = this.state;
    return (
      <div className="react-check-box">
        <div className="React__checkbox" onClick={() => this.handleCheckBoxChange()}>
          <label htmlFor="checkbox" className="check-box-label">
            <input
              name="checkbox"
              type="checkbox"
              className="React__checkbox--input"
              checked={checked}
              disabled={disabled}
              onChange={() => { }}
            />

            <span
              className="React__checkbox--span"
            />
            <div className="d-inline-flex">
              {this.props.label ? <div className="checkbox-label">{this.props.label}</div> : ''}
              {this.props.labelType ? <div className="checkbox-label-type">{this.props.labelType}</div> : ''}
            </div>
          </label>
        </div>
        {/* {this.props.label ? <div className="checkbox-label">{this.props.label}</div> : ''} */}
      </div>

    );
  }
}

export default CheckBoxComponent;
