import React, { PureComponent } from 'react';
import { ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import PropTypes from 'prop-types';

export default class PublishSmartFrameComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    console.log('PublishSmartFrameComponent', this.props);
    const { data } = this.props;
    return (
      <div className="publish-smart-frame">
        <ModalHeader toggle={this.props.toggle} >Publish SmartFrame</ModalHeader>
        <ModalBody>
          <div className="row">
            <div className="col-sm-12 text-font-size p-2 text-center">
              <i> You are going to publish <b>"SmartFrame"</b> with the next numbers:</i>
            </div>
          </div>
          <div className="row">
            <div className="col-sm-12 col-md-4 p-1">
              <div className="card shadow">
                <div className="justify-content-between text-center">
                  <div className="header-font-size text-theme-color align-middle px-2 pt-2">
                    <h4>{data.FileSize}</h4>
                  </div>
                  <div className="text-font-size px-1 pb-1">
                    Size
                  </div>
                </div>

              </div>
            </div>
            <div className="col-sm-12 col-md-4 p-1">
              <div className="card shadow">
                <div className="justify-content-between text-center">
                  <div className="header-font-size text-theme-color align-middle px-2 pt-2">
                    <h4>{data.Rows}</h4>
                  </div>
                  <div className="text-font-size px-1 pb-1">
                    Rows
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-12 col-md-4 p-1">
              <div className="card shadow">
                <div className="justify-content-between text-center">
                  <div className="header-font-size text-theme-color align-middle px-2 pt-2">
                    <h4>{data.Frames}</h4>
                  </div>
                  <div className="text-font-size px-1 pb-1">
                    Frame
                  </div>
                </div>
              </div>
            </div>

          </div>
          <div className="row">
            <div className="col-sm-12 col-md-12 p-2 mt-2 d-flex download-smart-frame">
              <a href={data.FilePath} className="btn btn-download"><i className="fa fa-download px-2 text-theme-color"></i>Download to Local Storage</a>
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <button color="secondary" className="btn btn-light btn-font-size mx-2" onClick={this.props.toggle}>Cancel</button>
          <button color="primary" className="btn btn-primary btn-done btn-font-size" onClick={this.props.toggle}>Done</button>
        </ModalFooter>
      </div>
    );
  }
}
PublishSmartFrameComponent.propTypes = {
  data: PropTypes.object,
  toggle: PropTypes.func,
};
