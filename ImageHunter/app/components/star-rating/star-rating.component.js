import React from 'react';
import StarRatings from 'react-star-ratings';

const StarRatingComponent = (props) => (
  <StarRatings
    rating={props.rating}
    starRatedColor="#FFD200"
    changeRating={props.onChangeRating}
    numberOfStars={5}
    starDimension="12px"
    starSpacing="2px"
  />
);

export default StarRatingComponent;
