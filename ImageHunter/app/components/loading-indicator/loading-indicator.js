import React from 'react';

import './loading-indicator.scss';

const LoadingIndicator = () => (
  <div>
    <div className="overlay">
    </div>
    <div className="overlay-body">
      <div className="loading-container">
        <div className="loading"></div>
        <div id="loading-text">SwitchBoardX</div>
      </div>
    </div>
  </div>
);

export default LoadingIndicator;
