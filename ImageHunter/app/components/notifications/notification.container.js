import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector, createSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import NotificationComponent from './notification.component';
import reducer from './notification.reducer';

const notificationSelector = (state) => (state.notificationStore || {});

const getNotifications = () => createSelector(
  notificationSelector,
  (notification) => {
    return { ...notification };
  }
);
const mapDispatchToProps = () => ({
});

const mapStateToProps = createStructuredSelector({
  notification: getNotifications()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'notificationStore', reducer });

const NotificationsContainer = compose(withReducer, withConnect)(NotificationComponent);
export default NotificationsContainer;
