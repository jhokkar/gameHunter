import { Tooltip } from 'reactstrap';
import React, { PureComponent } from 'react';

class TooltipComponent extends PureComponent {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            tooltipOpen: false
        };
    }
    toggle() {
        this.setState({
            tooltipOpen: !this.state.tooltipOpen
        });
    }
    render() {
        return (
          <Tooltip
            placement={this.props.placement}
            isOpen={this.state.tooltipOpen}
            target={this.props.targetId}
            toggle={this.toggle}
            autohide={false}

          >
              {this.props.text}
            </Tooltip>
        );
    }
}

export default TooltipComponent;
