import React from 'react';
import PropTypes from 'prop-types';
import './custom-badge.scss';

const CustomBadge = (props) => {
  return (<span className="badge custom-badge">{props.label}</span>);
};

CustomBadge.propTypes = {
  label: PropTypes.string,
};
export default CustomBadge;
