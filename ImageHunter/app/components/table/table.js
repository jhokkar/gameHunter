import { DataTable } from 'primereact/components/datatable/DataTable';
import { Column } from 'primereact/components/column/Column';
import React, { PureComponent } from 'react';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/omega/theme.css';


import './table.scss';

class TableComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  getColumns(tableData) {
    const columns = [];
    const keys = Object.keys(tableData[0]);
    keys.forEach((propertyName) => {
      const column = {};
      // column.Header = capitalizeFirstLetter(propertyName);
      // column.accessor = propertyName;
      column.header = propertyName;
      column.field = propertyName;
      columns.push(column);
    });
    return columns;
  }

  render() {
    return (
      <DataTable value={this.props.tableData} columnResizeMode="expand" scrollable={true} scrollHeight="calc(100vh - 300px)">
        {this.getColumns(this.props.tableData).map((col) => (

          <Column className="column-division" key={col.field} field={col.field} header={col.header} />
        ))}
      </DataTable>
    );
  }
}

export default TableComponent;
