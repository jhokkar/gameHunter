import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


class Facet extends PureComponent {
    constructor(props) {
        super(props);
    }
    render() {
        return (
          <span onClick={this.props.toggleFacet} className={this.props.facet.isActive ? "badge custom-badge-info active cursor-pointer" : "badge custom-badge-info cursor-pointer"}>{this.props.facet.filterDisplayName}</span>
        );
    }

}

export default Facet;
