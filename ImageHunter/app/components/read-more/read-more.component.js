/**
 * Read More component
 * react-simple-read-more-Module
 */
import React, { Component } from 'react';
import Linkify from 'react-linkify';
import { Link } from 'react-router-dom';

class ReadMore extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isMore: false,
      text: []
    };
  }

  static defaultProps = {
    charLimit: 50
  };

  componentDidMount() {
    this.createTextArr(this.props);
  }

  componentWillReceiveProps(nextProp) {
    if (nextProp.text != this.props.text) {
      this.createTextArr(nextProp);
    }
  }

  createTextArr(nextProp) {
    let text = [];
    let result = [];
    if (nextProp.text) text = nextProp.text.split("\n");
    for (let i = 0; i < text.length; i++) {
      result.push({
        text: text[i],
        newLine: true
      });
      // let wordList = text[i].split(' ');
      let tempText = text[i];
      let tempArr = [];
      let cmptext = '';
      tempArr.push(tempText);
      while (tempText.length > this.props.charLimit && (!tempArr.length || tempArr[0] != cmptext)) {
        cmptext = tempArr[0];
        tempText = tempArr[0].substr(0, tempArr[0].length / 2);
        for (let j = 0; j < tempArr.length; j = j + 2) {
          let splitIndex = tempArr[j].length / 2;
          while (tempArr[j].charAt(splitIndex) != ' ' && splitIndex < tempArr[j].length) {
            splitIndex++;
          }
          tempArr.splice(j + 1, 0, tempArr[j].substr(splitIndex));
          tempArr.splice(j, 1, tempArr[j].substr(0, splitIndex));
        }
      }
      text.splice(i, 1, tempArr[0]);
      result.splice(i, 1, {
        text: tempArr[0],
        newLine: tempArr.length == 1 ? true : false
      });
      for (let j = 1; j < tempArr.length; j++) {
        text.splice(i + j, 0, tempArr[j]);
        result.splice(i + j, 0, {
          text: tempArr[j],
          newLine: j == (tempArr.length - 1) ? true : false
        });
        i++;
      }
      //     if(wordList.length) {
      //         let nextWords = wordList[0];
      //         for (let j = 1; j< wordList.length; j++) {
      //             if(wordList[i]=='') {

      //             } else if (j == 10) {
      //                 text[i] = nextWords
      //             } else if(j%10 == 0) {

      //             }
      //         }
      //     }
    }
    this.setState({ text: result });
  }

  render() {
    return (
      <div className={this.state.isMore ? this.props.classes : `${this.props.classes} custom-text-truncate`} style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>
        {this.state.text.map((station, i) => {
          if (station.text == '' && (i < this.props.breakOn || this.state.isMore)) {
            return <div style={{ height: 10 }} className="" key={i}>{station.text.replace(/ /g, "\u00a0")}</div>;
          } else if (i < this.props.breakOn || this.state.isMore) {
            return station.newLine ? <span key={i}><Linkify properties={{ target: '_blank' }} target="_blank">{station.text}</Linkify><div></div></span> : <Linkify properties={{ target: '_blank' }} target="_blank" key={i}>{station.text}</Linkify>;
          } else if (i == this.props.breakOn) {
            if (this.props.isTextNeeded) {
              if (this.props.link) {
                return (
                  <Link to={this.props.link}>
                    <span
                      key={i}
                      className='cursor-pointer read-more-link'
                    >
                      ..More
                    </span>
                  </Link>)
              } else {
                return (<span
                  key={i}
                  className='cursor-pointer read-more-link'
                  onClick={() => { this.setState({ isMore: true }); }}
                >
                  ..More
              </span>)
              };

            } else {
              return (<span key={i} className='read-more-link'> ...</span>);

            }
          }
        })}
        {this.state.isMore ?
          <div className='cursor-pointer show-less-link' onClick={() => { this.setState({ isMore: false }); }}> ..less</div>
          :
          null
        }
      </div>
    );
  }
};

export default ReadMore;