

import React from 'react';
import './custom-spinner.scss';

export default class Spinner extends React.Component {
    componentWillReceiveProps(nextProps) {
        this.setState({
            ...nextProps
        });
    }
    render() {
        return (
            <div>
                {(this.props.spinner.pendingTasks > 0) &&
                    <div>
                        <div className="overlay">
                        </div>
                        <div className="overlay-body">
                            <div className="loading-container">
                                <div className="loading"></div>
                                <div id="loading-text">SwitchBoardX</div>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}
