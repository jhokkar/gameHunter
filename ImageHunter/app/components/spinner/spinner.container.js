import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import { compose } from 'redux';

import SpinnerComponent from './spinner.component';
import spinnerReducer, { BeginTask, EndTask, EndAllTasks } from './spinner.reducer';
import { createStructuredSelector, createSelector } from 'reselect';

const mapDispatchToProps = (dispatch) => ({
  StartPendingTask: () => {
    dispatch(BeginTask());
  },
  StopPendingTask: () => {
    dispatch(EndTask());
  },
  StopAllPendingTasks: () => {
    dispatch(EndAllTasks());
  }
});

const spinStore = (state) => state.spinnerStore;

const makeSpin = () => createSelector(
  spinStore,
  (spinner) => spinner
);
const mapStateToProps = createStructuredSelector({
  spinner: makeSpin(),
});



const withConnect = connect(mapStateToProps, mapDispatchToProps);
const withReducer = injectReducer({ key: 'spinnerStore', reducer: spinnerReducer });
const SpinnerContainer = compose(withReducer, withConnect)(SpinnerComponent);

export default SpinnerContainer;
