
const LOADER = 'LOADER';

export function BeginTask() {
  return (dispatch, getState) => {
    let pendingTasks = getState().spinnerStore.pendingTasks;
    pendingTasks = pendingTasks + 1;
    dispatch({
      type: LOADER,
      pendingTasks
    });
  };
}

export function EndTask() {
  return (dispatch, getState) => {
    let pendingTasks = getState().spinnerStore.pendingTasks;
    pendingTasks = pendingTasks === 0 ? pendingTasks : pendingTasks - 1;
    dispatch({
      type: LOADER,
      pendingTasks
    });
  };
}

export function EndAllTasks() {
  return (dispatch) => {
    dispatch({
      type: LOADER,
      pendingTasks: 0
    });
  };
}

const initialState = {
  pendingTasks: 0,
  BeginTask,
  EndTask,
  EndAllTasks
};

function spinnerReducer(state = initialState, action) {
  switch (action.type) {
    case LOADER: {
      return Object.assign({}, state, {
        pendingTasks: action.pendingTasks
      });
    }
    default:
      return state;
  }
}

export default spinnerReducer;
