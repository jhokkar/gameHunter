import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector, createSelector } from 'reselect';

import HeaderComponent from './header.component';
import { searchDataBasedOnInput, updateHeaderSearchInput } from './header.actions';
import { makeSelectSearchInput } from '../../dashboard/dashboard.selector';
import { makeSelectLoading, makeSelectError, checkForDashboardRoute } from '../../layout/layout.selectors';
import injectReducer from 'utils/injectReducer';
import reducer from './header.reducer';


const getHeaderSearchInput = () => createSelector(
  (state) => state.header.headerSearchInput,
  (headerSearchInput) => headerSearchInput || ''
);


const mapDispatchToProps = (dispatch) => ({
  onSearchClick: (value) => dispatch(searchDataBasedOnInput(value)),
  onHeaderSearchInputChange: (value) => dispatch(updateHeaderSearchInput(value))
});

const mapStateToProps = createStructuredSelector({
  searchInput: makeSelectSearchInput(),
  isDashBoard: checkForDashboardRoute(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
  headerSearchInput: getHeaderSearchInput()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'header', reducer });
const HeaderContainer = compose(withReducer, withConnect)(HeaderComponent);

export default HeaderContainer;
