import React, { PureComponent } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import { Link } from 'react-router-dom';

import Logo from '../../assets/images/switchboardx.png';
import './header.scss';
import {
  withRouter
} from 'react-router-dom'
class HeaderComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.handleKeyPressEvent = this.handleKeyPressEvent.bind(this);
    this.onSearchInputChange = this.onSearchInputChange.bind(this);
    this.onSearchButtonClick = this.onSearchButtonClick.bind(this);
    this.state = {
      isOpen: false,
    };
  }

  handleKeyPressEvent(event) {
    var code = event.keyCode || event.which;
    if (code === 13) {
      this.props.history.push('/dashboard')
      this.props.onSearchClick()
    }
  }

  onSearchInputChange(event) {
    this.props.onHeaderSearchInputChange(event.target.value);
  }

  onSearchButtonClick() {
    this.props.history.push('/dashboard');
    this.props.onSearchClick();
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render() {
    return (
      <div className="header">
        <Navbar color="light" light expand="md" className="fixed-top navbar-switchboard">
          <Link to="/">
            <img src={Logo} alt="SwitchBoardX-Logo" />
          </Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            {this.props.isDashBoard ?
              <div className="input-group shadow-sm col-md-10 col-sm-12 m-2">
                <div className="input-group-prepend">
                  <span className="input-group-text header-search header-search-icon"><i className="fa fa-search"></i></span>
                </div>
                <input
                  type="text"
                  placeholder="Search for data (e.g. dollar sales) "
                  className="form-control header-search-input"
                  onKeyPress={(event) => this.handleKeyPressEvent(event)}
                  onChange={this.onSearchInputChange}
                  value={this.props.headerSearchInput}
                />
                <div className="input-group-append">
                  <a href='javascript:void(0)' onClick={() => this.onSearchButtonClick()}>
                    <span className="input-group-text header-search header-search-text">SEARCH </span>
                  </a>
                </div>
              </div>
              :
              ''
            }
            <Nav className="ml-auto" navbar>
              {/* <NavItem>
                <NavLink className="p-1"><span className="cart fa fa-stack fa-shopping-cart has-badge" data-count="4"></span>
                </NavLink>
              </NavItem> */}
              <NavItem className="mx-2 text-truncate">
                <NavLink><i className="fa fa-user-circle"></i> Doug</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>

      </div>
    );
  }
}

export default withRouter(HeaderComponent);
