import { UPDATE_HEADER_SEARCH_INPUT } from '../../constants/constants';
import { clearAvailableDataSetNames,clearSelectedFacets } from '../../dashboard/dashboard.actions.js';

export function searchDataBasedOnInput() {
  return (dispatch, getState) => {
    const searchForDataMethod = getState().dashboard.searchForData;
    let searchInput = getState().header.headerSearchInput;
    // On empty search input load all the details by searching with '*'
    if (searchInput === '') {
      searchInput = '*';
    }
    dispatch(clearSelectedFacets());
    dispatch(clearAvailableDataSetNames());
    dispatch(searchForDataMethod(searchInput, [], ''));
  };
}

export function updateHeaderSearchInput(searchInput) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_HEADER_SEARCH_INPUT,
      payload: searchInput
    }
    );
  };
}
