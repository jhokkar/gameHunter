
import { UPDATE_HEADER_SEARCH_INPUT } from '../../constants/constants';


// The initial state of the App
const initialState = ({
    headerSearchInput: ''
});

function headerReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_HEADER_SEARCH_INPUT: {
            return { ...state, headerSearchInput: action.payload };
        }

        default:
            return state;
    }
}

export default headerReducer;
