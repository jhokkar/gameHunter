import React from 'react';

import './footer.scss';

const Footer = () => (
  <footer className="footer">
    <ul className="list-inline">
      <li className="list-inline-item"> © SwitchboardX 2017</li>
      <li className="list-inline-item">Have any questions?</li>
      <li className="list-inline-item">info@switchboardx.com</li>
    </ul>
  </footer>
);

export default Footer;
