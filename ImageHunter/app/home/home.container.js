import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from './home.reducers';
import { saveUserDetails } from './home.actions';
import {  } from './home.selector';
import HomeComponent from './home.component';

const mapDispatchToProps = (dispatch) => ({
    saveUserDetails: (user) => dispatch(saveUserDetails(user)),
});

const mapStateToProps = createStructuredSelector({
    // publishedSmartFrames: getPublishedSmartFrames()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });

const HomeContainer = compose(withReducer, withConnect)(HomeComponent);
export default HomeContainer;
