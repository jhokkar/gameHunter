/*
 * Related Variable Reducer
 */
import { BIND_RELATED_VAR_RESULTS, BIND_RELATED_VAR_PREVIEW_DATA } from '../constants';

const initialState = ({
    selectedRelatedVarName: '',
    relatedVarDetails: {
        ContextVariables: [],
        ComplementaryVariables: []
    },
    previewData: []
});

function relatedVariableDetailsReducer(state = initialState, action) {
    switch (action.type) {
        case BIND_RELATED_VAR_RESULTS: {
            return { ...state, selectedRelatedVarName: action.payload.selectedRelatedVarName, relatedVarDetails: action.payload.relatedVarDetails };
        }
        case BIND_RELATED_VAR_PREVIEW_DATA: {
            return { ...state, previewData: [...action.payload.previewData] };
        }

        default:
            return state;
    }
}

export default relatedVariableDetailsReducer;
