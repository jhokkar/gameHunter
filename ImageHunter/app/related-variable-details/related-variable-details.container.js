import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import RelatedVariablesDetailsComponent from './related-variable-details.component';
import { getRelatedVarDetails, getPreviewData } from './related-variable-details.selector';
import injectReducer from 'utils/injectReducer';
import reducer from './related-variable-details.reducers';
import { loadRelatedVariablesDetails, loadPreviewData } from './related-variable-details.actions';

const mapDispatchToProps = (dispatch) => ({
    loadRelatedVariablesDetails: () => dispatch(loadRelatedVariablesDetails()),
    loadPreviewData: (relatedVarName,dataSourceName) => dispatch(loadPreviewData(relatedVarName,dataSourceName))
});

const mapStateToProps = createStructuredSelector({
    relatedVarDetails: getRelatedVarDetails(),
    previewData: getPreviewData()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'relatedVarDetails', reducer });

const RelatedVariablesDetailsContainer = compose(withReducer, withConnect)(RelatedVariablesDetailsComponent);
export default RelatedVariablesDetailsContainer;
