import { BIND_RELATED_VAR_RESULTS, BIND_RELATED_VAR_PREVIEW_DATA } from '../constants';
import { getRelatedVarDetailsBasedOnName, getRelatedVarDetailsPreviewDataByName } from './related-variable-details.service';
import { ResponseStatusEnum } from '../constants/enums/response-status-enum';

export function loadRelatedVariablesDetails() {
    return (dispatch, getState) => {
        const relatedVarIdQuery = getState().route.location.search;
        if (relatedVarIdQuery) {
            let relatedVarName = relatedVarIdQuery.split('=')[1];
            dispatch(getState().spinnerStore.BeginTask());

            getRelatedVarDetailsBasedOnName(decodeURI(relatedVarName)).then((response) => {

                dispatch({
                    type: BIND_RELATED_VAR_RESULTS,
                    payload: {
                        selectedRelatedVarName: relatedVarName,
                        relatedVarDetails: response.data && response.data.length > 0 ? response.data[0] : {
                            ContextVariables: [],
                            ComplementaryVariables: []
                        },
                    }
                });
                dispatch(getState().spinnerStore.EndTask());

            }).catch((e) => {
                console.log('getRelatedVarDetailsBasedOnName', e);
                dispatch(getState().spinnerStore.EndTask());
                dispatch(getState().notificationStore.notify('Failed to fetch Related Variable Details', ResponseStatusEnum.Error));

            });
        };
    };
}

export function loadPreviewData(relatedVarName, datasource) {
    return (dispatch, getState) => {
        dispatch(getState().spinnerStore.BeginTask());
        getRelatedVarDetailsPreviewDataByName(relatedVarName, datasource).then((response) => {
            dispatch({
                type: BIND_RELATED_VAR_PREVIEW_DATA,
                payload: {
                    previewData: response.data
                }
            });
            dispatch(getState().spinnerStore.EndTask());

        }).catch((e) => {
            console.log('getRelatedVarDetailsPreviewDataByName', e);
            dispatch(getState().spinnerStore.EndTask());
            dispatch(getState().notificationStore.notify('Failed to fetch Preview data', ResponseStatusEnum.Error));
        });
    };
}

