/**
 * DashBoard selectors
 */

import { createSelector } from 'reselect';

const selectedRelatedVarSelector = (state) => (state.relatedVarDetails ? state.relatedVarDetails.selectedRelatedVarName : 0);
const selectedRelatedVarDetailsSelector = (state) => (state.relatedVarDetails ? state.relatedVarDetails.relatedVarDetails : {});
const previewDataSelector = (state) => (state.relatedVarDetails ? state.relatedVarDetails.previewData : []);

const makeSelectedRelatedVar = () => createSelector(
    selectedRelatedVarSelector,
    (selectedRelatedVarName) => selectedRelatedVarName || ''
);

const getRelatedVarDetails = () => createSelector(
    selectedRelatedVarDetailsSelector,
    (selectedRelatedVarDetails) => {
        return selectedRelatedVarDetails || {};
    }
);
const getPreviewData = () => createSelector(
    previewDataSelector,
    (previewData) => previewData.map((preview) => ({ ...preview }))
);

export {
    makeSelectedRelatedVar,
    getRelatedVarDetails,
    getPreviewData
};
