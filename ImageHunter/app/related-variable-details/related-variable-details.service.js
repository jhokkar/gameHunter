import { axiosLocalGet, axiosPost } from '../utils/axios-api';

export function getRelatedVarDetailsBasedOnName(relatedVarName) {
     const data = {
        data: {
            RelatedVariableName: relatedVarName,
        }
    };
    return axiosPost('related-var-details', data);
}

export function getRelatedVarDetailsPreviewDataByName(relatedVariableName,DataSource) {
    const data = {
        data: [{
            "DataSource": DataSource,
            "RelatedVariables":[relatedVariableName],
            "Variables":[]
        }]
    };
    return axiosPost('preview-data', data);
}
