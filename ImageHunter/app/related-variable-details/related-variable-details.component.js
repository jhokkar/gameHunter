/*
 * Data Source Details Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import ReactDrawer from 'react-drawer';
import PropTypes from 'prop-types';
import CustomBadge from 'components/custom-badge';
import './related-variable-details.scss';
import { Link } from 'react-router-dom';
import PreviewDataComponent from '../datasource-details/components/preview-data.component';

class RelatedVariablesDetailsComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      isViewDataOpen: false
    };
  }
  componentDidMount() {
    this.props.loadRelatedVariablesDetails();
  }

  render() {
    const { relatedVarDetails } = this.props;
    console.log('relatedVarDetails', relatedVarDetails, this.props.previewData);
    return (
      <section>
        <Helmet>
          <title>Related Variable Details | SwitchBoardX</title>
          <meta name="description" content="Related Variable Details, SwitchBoardX" />
        </Helmet>
        <div className="datasource-details-page">
          <div className="row shadow-sm mb-4">
            <div className="px-2 d-inline-flex">
              <div >
                <Link to="/dashboard">
                  <i className="fa fa-2x half-opacity fa-chevron-circle-left"></i>
                </Link>
              </div>
              <div className="py-1 ml-1">
                <h5>Related Variable Details</h5>
              </div>
            </div>
          </div>
          <div className="row datasource-overview-details">
            <div className="col-sm-4 col-md-2 col-12">
              <div className="datasource-icon shadow py-1">
                {(relatedVarDetails.ProviderLogo ?
                  <img src={relatedVarDetails.ProviderLogo} alt="data-source-logo" />
                  :
                  <i className="fa text-theme-color fa-custom fa-file"></i>
                )
                }
              </div>
            </div>
            <div className="datasource-provider-name-overview  col-sm-12 col-md-10 ">
              <div className="row no-gutters">
                <div className="col-md-4">
                  <h4 className="font-weight-bold mt-1">
                    {relatedVarDetails.Name || 'No Data Found'}
                  </h4>
                  <span className="text-muted">
                    {relatedVarDetails.Category}
                  </span>
                </div>
                <div className="offset-md-5 col-md-3">
                  <button
                    onClick={() => {
                      this.props.loadPreviewData(relatedVarDetails.Name,relatedVarDetails.DataSourceName);       //add datasource as second param input
                      // triggered on "<" on left top click or on outside click
                      this.setState({ isViewDataOpen: true });
                    }}
                    className="btn btn-white shadow header-font-size font-weight-bold"
                  >
                    VIEW DATA
                  </button>
                </div>
              </div>
              <hr />
            </div>
          </div>
          <div className="row no-gutters">
            <div className="datasource-data-details p-2 d-flex-inline col-12 col-sm-9 ">
              <div className="row no-gutters" >
                <div className="col-12">
                  <span className="detail-header text-muted">
                    Description:
                  </span>
                  <span className="detail-content">
                    {relatedVarDetails.LongDescription}
                  </span>
                </div>
                <div className="col-12">
                  <span className="detail-header  text-muted">

                    Data Source Details: <b>{relatedVarDetails.DataSourceName}</b>
                  </span>
                  <span className="detail-content">
                    {relatedVarDetails.DataSourceDescription}
                  </span>
                </div>
                <div className="col-12">
                  <span className="detail-header text-muted ">
                    Provider Information: <b>{relatedVarDetails.ProviderName}</b>
                  </span>
                  <span className="detail-content">
                    {relatedVarDetails.ProviderDescription}
                  </span>
                </div>
              </div>
            </div>
            <div className="datasource-details-sidebar p-1 col-12 col-sm-3 ">
              <div className="row ml-1 no-gutters">
                <div className="col-12">
                  <span className="detail-header   text-muted">
                    Contextual Variables:
                  </span>
                  <span className="detail-content">
                    {relatedVarDetails.ContextVariables && relatedVarDetails.ContextVariables.map((variable, index) => (
                      <CustomBadge label={variable[1]} key={variable[0]} ></CustomBadge>
                    ))
                    }
                    {!relatedVarDetails.ContextVariables ? 'No Records Found' : ''}
                  </span>
                </div>
              </div>
            </div>
          </div>
          <ReactDrawer
            open={this.state.isViewDataOpen}
            position="bottom"

            onClose={() => {
              // triggered on "<" on left top click or on outside click
              this.setState({ isViewDataOpen: false });
            }}
          >
            <div className="preview-data">
              <div className="clear-both">
                <i
                  onClick={() => {
                    this.setState({ isViewDataOpen: false });
                  }}
                  className="fa cursor-pointer fa-times pull-right p-1"
                >
                </i>
              </div>
              {this.props.previewData && this.props.previewData.length > 0 ?
                <div>
                  <PreviewDataComponent data={this.props.previewData} />
                  <div className="row">
                    <div className="col-12"> <p className="text-font-size px-2">Note: Top 200 records are shown</p></div>
                  </div>
                </div>
                : <div className="row"><div className="col-12"><h5 className="p-5">No Records Found</h5></div></div>
              }
            </div>
          </ReactDrawer>
        </div>
      </section>
    );
  }
}
RelatedVariablesDetailsComponent.propTypes = {
  loadRelatedVariablesDetails: PropTypes.func,
  loadPreviewData: PropTypes.func,
  relatedVarDetails: PropTypes.object,
  previewData: PropTypes.array
};
export default RelatedVariablesDetailsComponent;
