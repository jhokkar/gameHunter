export function getImageSource(){
    return (dispatch,getState) => {
        return getState().trainer.imageSource;
    }
}

export function setImageSource(source){
    return (dispatch,getState) => {
        dispatch({
            type: "SET_IMAGE_SOURCE",
            payload: {
                ImageSource:source
            }
        });
    }
}