import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap';
import ReactDrawer from 'react-drawer';
/* if you using webpack, should not apply identity to this css */
import 'react-drawer/lib/react-drawer.css';

import './dashboard.scss';
import RelatedVariablesContainer from './related-variables';
import SelectedDataItemsContainer from './selected-data-items';
import SelectedRelatedVariablesContainer from './selected-related-variables';
import DataSourcesContainer from './data-sources';
import SearchFilterComponent from './search-filter/search-filter.container';
import TooltipComponent from '../components/tool-tip/tool-tip.component';
import SmartFramesContainer from './smart-frames';

export default class DashboardComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    // Internal actions, No need of Redux Store usage
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      isPaneOpen: false
    };
  }
  componentDidMount() {
    if (!(this.props.relatedVariables.length > 0)) {
      // On Page Load , load ll the details by searching with '*'
      this.props.onSearch('*', [], '');
    }
  }
  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <section>
        <Helmet>
          <title>Home | SwitchBoardX</title>
          <meta name="description" content="Home, SwitchBoardX" />
        </Helmet>
        <div className="home-page">
          <div className="tabs-wrapper">
            <div className="row design-smart-frame">
              <div className="col-md-7 col-xs-12 d-inline order-sm-1  order-xs-1 order-md-0 fixed-nav-tabs">
                <Nav tabs className="">
                  <NavItem>
                    <NavLink
                      className={this.state.activeTab === '1' ? 'active' : ''}
                      onClick={() => { this.toggle('1'); }}
                    >
                      Related Variables
                  </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={this.state.activeTab === '2' ? 'active' : ''}
                      onClick={() => { this.toggle('2'); }}
                    >
                      Data Sources
                  </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      className={this.state.activeTab === '3' ? 'active' : ''}
                      onClick={() => { this.toggle('3'); }}
                    >
                      Smart Frames
                  </NavLink>
                  </NavItem>
                </Nav>
              </div>
              <div className="col-md-5 order-sm-0 order-xs-0 order-md-1 col-sm-12 pull-right align-items-center justify-content-end smart-frame-div d-flex">
                <div className="selected-datasource-text">
                  <SelectedDataItemsContainer />
                </div>
                <div className="px-1 mr-2">
                  <Link to="/recommended-data">
                    <input
                      type="button"
                      value="DESIGN SMART FRAME"
                      className="btn-design-smart-frame btn btn-primary btn-font-size btn-responsive"
                    />
                  </Link>
                </div>
                <div className="">
                  <div className="d-md-none d-inline react-drawer-scroll">
                    <a className=" mt-2" id="filterToggle" href="javascript:void(0)" onClick={() => this.setState({ isPaneOpen: true })}>
                      <i className="fa fa-1x fa-align-justify"></i>
                    </a>
                    <TooltipComponent
                      placement="top"
                      targetId="filterToggle"
                      text="Filters"
                    />
                    <ReactDrawer
                      open={this.state.isPaneOpen}
                      position="right"
                      onClose={() => {
                        // triggered on "<" on left top click or on outside click
                        this.setState({ isPaneOpen: false });
                      }}
                    >
                      <div className="px-2 filter-sidebar" >
                        {this.props.searchInput !== '' ?
                          <SearchFilterComponent />
                          : ''
                        }
                      </div>

                    </ReactDrawer>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row no-gutters">
            <div className="col-md-9 col-xs-12 d-inline">

              <TabContent activeTab={this.state.activeTab} className="fixed-nav-tab-content">
                <TabPane tabId="1">
                  <div className="searched-data">
                    {
                      this.props.areRelatedVariablesFiltered ?
                        <SelectedRelatedVariablesContainer />
                        : null
                    }
                    {
                      this.props.relatedVariables.length > 0 ?
                        <RelatedVariablesContainer />
                        :
                        (this.props.searchInput != '' ?
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center">
                                  <i className="fa fa-3x fa-table"></i>
                                </span>
                                <p className="text-center"> No Records Found</p>
                              </div>
                            </div>
                          </div>
                          :
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center">
                                  <i className="fa fa-3x fa-table"></i>
                                </span>
                                <p className="text-center"> Please search for Related Variables</p>
                              </div>
                            </div>
                          </div>
                        )
                    }


                  </div>
                </TabPane>
                <TabPane tabId="2">
                  <div className="searched-data">
                    {
                      this.props.dataSources.length > 0 ?
                        <DataSourcesContainer />
                        : (this.props.searchInput !== '' ?
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center">
                                  <i className="fa fa-3x fa-table"></i>
                                </span>
                                <p className="text-center"> No Records Found</p>
                              </div>
                            </div>
                          </div>
                          :
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center"><i className="fa fa-3x fa-archive"></i></span>
                                <p className="text-center"> Please search for Data Sources</p>
                              </div>
                            </div>
                          </div>
                        )
                    }
                  </div>
                </TabPane>
                <TabPane tabId="3">
                  <div className="searched-data">
                    {
                      this.props.smartFrames.length > 0 ?
                        <SmartFramesContainer />
                        : (this.props.searchInput !== '' ?
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center">
                                  <i className="fa fa-3x fa-align-left"></i>
                                </span>
                                <p className="text-center"> No Records Found</p>
                              </div>
                            </div>
                          </div>
                          :
                          <div className="row">
                            <div className="col-12">
                              <div className="empty-data p-5">
                                <span className="text-center">
                                  <i className="fa fa-3x fa-align-left"></i>
                                </span>
                                <p className="text-center"> Please search for Smart Frames</p>
                              </div>
                            </div>
                          </div>
                        )
                    }
                  </div>
                </TabPane>
              </TabContent>
            </div>
            <div className="d-none d-md-block">
              {this.props.searchInput !== '' ?
                <SearchFilterComponent />
                : ''
              }
            </div>

          </div>
        </div>
      </section >
    );
  }
}

DashboardComponent.propTypes = {
  dataSources: PropTypes.array,
  searchInput: PropTypes.string,
  onSearch: PropTypes.func,
  relatedVariables: PropTypes.array,
  smartFrames: PropTypes.array
};
