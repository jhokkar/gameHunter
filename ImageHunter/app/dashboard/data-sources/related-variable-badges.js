import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
const MAX_ITEMS = 7;

class RelatedVariablesBadges extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { isOpen: false };
        this.getAssociatedClasses = this.getAssociatedClasses.bind(this);
    }
    getRenderedItems() {
        let reOrderedListOfRVs = [];
        const dsID = this.props.dataSourceID;
        // selected
        this.props.selectedDataItems.map((selectedDataItem) => {
            if(selectedDataItem.ID == dsID && this.props.data.length != 0 ){
                let dataItemConvertedToRV = _.find(this.props.data, (rv) => rv.RelatedVariableId == selectedDataItem.RelatedVarID)
                if(dataItemConvertedToRV){
                    reOrderedListOfRVs.push(dataItemConvertedToRV);
                }
            }
        });
        let remainingRVs = _.difference(this.props.data, reOrderedListOfRVs);
        _.forEach(remainingRVs, (rv)=> {if(rv) {reOrderedListOfRVs.push(rv)}});
        if (this.state.isOpen) {
            return reOrderedListOfRVs;
        }
        return reOrderedListOfRVs.slice(0, MAX_ITEMS);
    }
    toggleMoreLess = () => {
        this.setState({ isOpen: !this.state.isOpen });
    }
    getAssociatedClasses = (variable) => {
        if (_.find(this.props.selectedDataItems, (selectedItem) => selectedItem.RelatedVarID == variable.RelatedVariableId)) {
            return "badge custom-badge mx-1 py-1 selected-rv"
        }
        else return "badge custom-badge mx-1 py-1"
    }
    render() {
        return (
            <span>
                {this.getRenderedItems().map((variable) => (
                    <span key={variable.RelatedVariableId.toString()} className={this.getAssociatedClasses(variable)}>
                        {variable.RelatedVariableName}
                    </span>

                ))}
                {this.props.data.length > MAX_ITEMS ?
                    <span className="cursor-pointer text-theme-color mx-1 py-1 badge custom-badge" onClick={this.toggleMoreLess}>
                        {this.state.isOpen ? '..less' : '..more'}
                    </span> : ''

                }
            </span>
        );
    }
}

RelatedVariablesBadges.propTypes = {
    data: PropTypes.array
};

export default RelatedVariablesBadges;
