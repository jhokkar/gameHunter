import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { toggleSelectedDataItems ,updateIsSelectedDataItemsChanged} from '../dashboard.actions';
import DataSourcesComponent from './data-sources.component';
import { getDataSources, getSelectedDataItems, isDataSourceSelectionToggled } from '../dashboard.selector';

const mapDispatchToProps = (dispatch) => ({
    toggleSelectedDataItems: (dataItem) => dispatch(toggleSelectedDataItems(dataItem)),
    updateIsSelectedDataItemsChanged :(value) => dispatch(updateIsSelectedDataItemsChanged(value))
});

const mapStateToProps = createStructuredSelector({
    dataSources: getDataSources(),
    selectedDataItems: getSelectedDataItems(),
    isDataSourceSelectionToggled: isDataSourceSelectionToggled()

});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const DataSourcesContainer = compose(withConnect)(DataSourcesComponent);

export default DataSourcesContainer;
