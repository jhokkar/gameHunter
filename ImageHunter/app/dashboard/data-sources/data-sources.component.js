import React, { PureComponent } from 'react';
import CheckBoxComponent from 'components/checkbox';
import PropTypes from 'prop-types';
import { ReadMore } from 'components/read-more';
import * as _ from 'lodash';
import RelatedVariablesBadges from './related-variable-badges';
import { Link } from 'react-router-dom';

class DataSourcesComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  isDataSourceChecked(id) {
    const isPresent = _.find(this.props.selectedDataItems, (o) => o.ID === id && o.type == "DataSet");
    return isPresent !== undefined;
  }
  render() {
    return (
      <div>
        {this.props.dataSources.map((dataSource) => (
          <div key={dataSource.DataSourceID.toString()} className="datasources-data">
            <div className="col-sm-12 col-md-12 col-lg-12 p-2">
              <div className="card shadow">
                <div className="row">
                  <div className="col-md-3">
                    <div className="datasource-icon">
                      {(dataSource.ProviderLogo ?
                        <img src={dataSource.ProviderLogo} alt="data-source-logo" />
                        :
                        <i className="py-3 fa fa-3x fa-file"></i>
                      )
                      }
                      {/* <div className="text-center">
                    <StarRatingComponent
                      rating={3.4}
                      onChangeRating={(e) => { console.log('start Rating', e); }}
                    />
                  </div>
                  <div className="text-center text-font-size">(123 reviews)</div> */}
                    </div>
                  </div>
                  <div className="col-md-9 px-1">
                    <div className="card-block px-1">
                      <h5 className="card-title header-font-size">
                        <Link to={`/datasource-details?id=${dataSource.Name.toString()}`} >
                          {dataSource.Name}
                        </Link>
                        <div className="pull-right">
                          <CheckBoxComponent
                            checked={this.isDataSourceChecked(dataSource.DataSourceID)}
                            disabled={false}
                            emitChange={(value) => {
                              this.props.toggleSelectedDataItems({
                                ID: dataSource.DataSourceID,
                                DataSourceName: dataSource.Name,
                                RelatedVarID: null,
                                RVName: null,
                                value,
                                type: 'DataSet'
                              });
                              this.props.updateIsSelectedDataItemsChanged(true);
                            }}
                          />
                        </div>
                      </h5>
                      <div className="d-flex justify-content-between">
                        <div className="card-title text-muted text-normal-font-size">
                          Provider: {dataSource.ProviderName}
                        </div>
                        <div className="card-title text-muted px-2 text-normal-font-size flex-grow-1">
                          Type: {dataSource.Category}
                        </div>
                      </div>
                      <div className="card-text text-font-size">
                        <ReadMore
                          text={dataSource.ShortDescription}
                          breakOn="1"
                          isTextNeeded
                          charLimit="250"
                        />
                      </div>
                      <hr />
                      <span className="text-muted header-font-size">Related Variables:</span>
                      {dataSource.RelatedVariables ?
                        <RelatedVariablesBadges data={dataSource.RelatedVariables} selectedDataItems={this.props.selectedDataItems} dataSourceID={dataSource.DataSourceID}/> : 'No Related Variables Found'}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))
        }
      </div>
    );
  }
}

DataSourcesComponent.propTypes = {
  selectedDataItems: PropTypes.array,
  dataSources: PropTypes.array,
  isDataSourceSelectionToggled: PropTypes.bool,
  toggleSelectedDataItems: PropTypes.func
};
export default DataSourcesComponent;
