/**
 * SelectedRelatedVariablesComponent
 */
import React, { PureComponent } from 'react';
import CheckBoxComponent from 'components/checkbox';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import 'components/react-awesome-popover/react-awesome-popover.scss';
import ReactAwesomePopover from 'components/react-awesome-popover/react-awesome-popover';
import { Motion, spring } from 'react-motion';
import * as _ from 'lodash';
import TooltipComponent from '../../components/tool-tip/tool-tip.component';

const Popover = ReactAwesomePopover;

class SelectedRelatedVariablesComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = { selectedRelatedDataItems: [] }
  }
  isVariableChecked(id) {
    const isPresent = _.find(this.props.selectedDataItems, (o) => o.RelatedVarID === id);
    return isPresent !== undefined;
  }

  componentDidMount(prevProps, prevState) {
    if (this.props.selectedDataItems) {
      this.updateRelatedVariables()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.selectedDataItems && prevProps.selectedDataItems != this.props.selectedDataItems) {
      this.updateRelatedVariables()
    }
  }

  updateRelatedVariables() {
    let values = [];
    this.props.selectedDataItems.map(item => {
      let selectedItem = _.find(this.props.relatedVariables, (rela) => {
        return item.RelatedVarID == rela.ID
      })

      values.push(selectedItem);
    })
    this.setState({
      selectedRelatedDataItems: values
    })
  }

  render() {
    const { relatedVariables, ...props } = this.props;
    return (
      <div className="related-variables-data">
        <div className="row py-1">
          {this.state.selectedRelatedDataItems.map((variableItem) => (
            <div key={variableItem.ID.toString()} className="col-sm-12 col-md-4 col-lg-3 col-xl-3 p-1">
              <div className="card shadow">
                <div className="card-header d-inline-flex justify-content-between font-size-normal variable-data-type">
                  <div className="text-truncate align-middle">
                    {variableItem.Category}
                  </div>
                  <div className="pull-right">
                    <CheckBoxComponent
                      checked={this.isVariableChecked(variableItem.ID)}
                      disabled={false}
                      emitChange={(value) => {
                        props.toggleSelectedDataItems({
                          ID: variableItem.DataSourceID,
                          DataSourceName: variableItem.DataSourceName,
                          RelatedVarID: variableItem.ID,
                          RVName: variableItem.Name,
                          value,
                          type: 'Related'
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="card-body">
                  <Link to={`/related-var-details?id=${variableItem.Name.toString()}`} className="no-href">
                    <h5
                      id={`varItem${variableItem.ID.toString()}`}
                      className={variableItem.IsAggregate ? 'text-theme-color card-title header-font-size' : 'card-title header-font-size'}
                    >
                      {variableItem.Name}
                    </h5>
                  </Link>
                  <TooltipComponent
                    placement="right"
                    targetId={`varItem${variableItem.ID.toString()}`}
                    text={variableItem.Name}
                  />
                  <div className={`card-text text-font-size variable-description varDesc${variableItem.ID.toString()}`} >
                    <Popover
                      motion
                      targetClass="custom-text-truncate body"
                      modifiers={{ flip: { behavior: 'clockwise' } }}
                      action="hover"
                      contentClass={` popover-width-100 varDescDetails${variableItem.ID.toString()}`}
                      placement="right"
                    >
                      {variableItem.ShortDescription}
                      {(popperProps, Arrow) => (
                        <Motion defaultStyle={{ rotateY: 90 }} style={{ rotateY: spring(0) }}>
                          {({ rotateY }) => {
                            const motionStyle = {
                              transform: `${popperProps.style.transform} rotateY(${rotateY}deg)`
                            };
                            return (
                              <div
                                {...popperProps}
                                style={{ ...popperProps.style, ...motionStyle }}
                              >
                                <div className={`popover-content shadow-sm varDescDetails${variableItem.ID.toString()}`}>
                                  <h3 className="popover-header font-size-normal">{variableItem.Name}</h3>
                                  <div className="popover-body">{variableItem.ShortDescription}</div>
                                </div>
                                {Arrow}
                              </div>
                            );
                          }}
                        </Motion>
                      )}
                    </Popover>
                  </div>
                </div>
                <div className="card-footer text-muted">
                  <div className="row no-gutters">
                    <div className="col-md-3 col-lg-4 text-center">
                      {(variableItem.ProviderLogo ?
                        <img src={variableItem.ProviderLogo} alt="data-source-logo" />
                        :
                        <i className="fa text-theme-color fa-custom fa-file"></i>

                      )
                      }
                    </div>
                    <div className="col-md-9 col-lg-8 custom-text-truncate text-font-size">
                      <Link to={`/datasource-details?id=${variableItem.DataSourceName.toString()}`}>
                        <span>
                          {variableItem.DataSourceName}
                          {/* <ReadMore
                            text={variableItem.provider}
                            breakOn="1"
                            isTextNeeded={false}
                            charLimit="38"
                          /> */}
                        </span>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
          }
        </div>
        <hr />
      </div>
    );
  }
}

SelectedRelatedVariablesComponent.propTypes = {
  relatedVariables: PropTypes.array,
  toggleSelectedDataItems: PropTypes.func,
  selectedDataItems: PropTypes.array,
  isDataSourceSelectionToggled: PropTypes.bool
};
export default SelectedRelatedVariablesComponent;
