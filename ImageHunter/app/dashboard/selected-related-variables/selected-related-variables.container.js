import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { toggleSelectedDataItems } from '../dashboard.actions';
import SelectedRelatedVariablesComponent from './selected-related-variables.component';
import { getRelatedVariables, getSelectedDataItems, isDataSourceSelectionToggled } from '../dashboard.selector';
import { onRelatedVariableClick } from './selected-related-component.actions';

const mapDispatchToProps = (dispatch) => ({
    toggleSelectedDataItems: (dataItem) => dispatch(toggleSelectedDataItems(dataItem)),
    onRelatedVariableClick: (variableId) => dispatch(onRelatedVariableClick(variableId))
});

const mapStateToProps = createStructuredSelector({
    relatedVariables: getRelatedVariables(),
    selectedDataItems: getSelectedDataItems(),
    isDataSourceSelectionToggled: isDataSourceSelectionToggled()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const SelectedRelatedVariablesContainer = compose(withConnect)(SelectedRelatedVariablesComponent);
export default SelectedRelatedVariablesContainer;
