export function onSelectRelatedVariable(selectedVariable) {
    return (dispatch, getState) => {
      const searchForDataMethod = getState().dashboard.searchForData;
      dispatch(searchForDataMethod(selectedVariable));
    };
  }
  
  export function onRelatedVariableClick(variableId) {
    return (dispatch) => {
      // console.log('variableId', variableId);
    };
  }
