/**
 * DashBoard selectors
 */

import { createSelector } from 'reselect';
import * as _ from 'lodash';

const searchInputState = (state) => (state.dashboard ? state.dashboard.searchInput : '');

const selectedDataItemsSelector = (state) => (state.dashboard ? state.dashboard.selectedDataItems : []);

const makeSelectSearchInput = () => createSelector(
  searchInputState,
  (searchInput) => searchInput || ''
);

const getRelatedVariables = () => createSelector(
  (state) => state.dashboard.relatedVariables,
  (relatedVariablesState) => relatedVariablesState.map((relatedVariable) => ({ ...relatedVariable }))
);

const getDataSources = () => createSelector(
  (state) => state.dashboard.dataSources,
  (dataSourcesState) => dataSourcesState.map((dataSource) => ({ ...dataSource }))
);
const getDistinctSelectedDataSets = () => createSelector(
  selectedDataItemsSelector,
  (selectedDataItems) => {
    let items = [];
    if (selectedDataItems) {
      items = _(selectedDataItems).groupBy((x) => x.ID)
        .map((value, key) => ({
          ID: key,
          DataSourceName: value[0].DataSourceName,
          RelatedVariables: value
        }))
        .value();
    }
    // const items = selectedDataItems ? selectedDataItems.map((selectedDataItem) => ({ ...selectedDataItem })) : [];
    return items;
  }
);

const getSelectedDataItems = () => createSelector(
  selectedDataItemsSelector,
  (selectedDataItems) => {
    const items = selectedDataItems ? selectedDataItems.map((selectedDataItem) => ({ ...selectedDataItem })) : [];
    return items;
  }
);

const getSmartFrames = () => createSelector(
  (state) => state.dashboard.smartFrames,
  (smartFrames) => smartFrames.map((smartFrame) => ({ ...smartFrame }))
);
const getAvailableFacets = () => createSelector(
  (state) => state.dashboard.availableFacets,
  (availableFacets) => (availableFacets && availableFacets.length > 0 ? availableFacets.map((availableFacet) => ({ ...availableFacet })) : [])
);
const getFacetSearchInput = () => createSelector(
  (state) => state.dashboard.facetSearchText,
  (facetSearchText) => facetSearchText || ''
);
const getSelectedFacets = () => createSelector(
  (state) => state.dashboard.selectedFacets,
  (selectedFacets) => selectedFacets.map((selectedFacet) => ({ ...selectedFacet }))
);

const isDataSourceSelectionToggled = () => createSelector(
  (state) => state.dashboard.isDataSourceSelectionToggled,
  (isToggled) => isToggled
);
const areRelatedVariablesFiltered = () => createSelector(
  (state) => state.dashboard.areRelatedVariablesFiltered,
  (isToggled) => isToggled
);
export {
  makeSelectSearchInput,
  getRelatedVariables,
  getDataSources,
  getSmartFrames,
  getSelectedDataItems,
  getSelectedFacets,
  getAvailableFacets,
  getDistinctSelectedDataSets,
  getFacetSearchInput,
  isDataSourceSelectionToggled,
  areRelatedVariablesFiltered
};
