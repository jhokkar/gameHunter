import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import {
    makeSelectLoading,
    makeSelectError
} from '../layout/layout.selectors';
import { searchForData, toggleSelectedDataItems } from './dashboard.actions';
import { makeSelectSearchInput, getRelatedVariables, getDataSources, getSmartFrames, areRelatedVariablesFiltered } from './dashboard.selector';
import reducer from './dashboard.reducer';
import DashboardComponent from './dashboard.component';

const mapDispatchToProps = (dispatch) => ({
    onSearch: (value, facets , clickedFacet) => dispatch(searchForData(value, facets, clickedFacet)),
    toggleSelectedDataItems: (dataItem) => dispatch(toggleSelectedDataItems(dataItem))
});

const mapStateToProps = createStructuredSelector({
    searchInput: makeSelectSearchInput(),
    loading: makeSelectLoading(),
    error: makeSelectError(),
    relatedVariables: getRelatedVariables(),
    dataSources: getDataSources(),
    smartFrames: getSmartFrames(),
    areRelatedVariablesFiltered: areRelatedVariablesFiltered()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'dashboard', reducer });
const DashBoardContainer = compose(withReducer, withConnect)(DashboardComponent);

export default DashBoardContainer;
