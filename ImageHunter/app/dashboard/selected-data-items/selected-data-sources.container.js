import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import SelectedDataItemsComponent from './selected-data-sources.component';
import { getDistinctSelectedDataSets } from '../dashboard.selector';
import { clearAllSelectedDataItemsFunction, areRelatedVariablesFilteredFunction } from '../dashboard.actions';

const mapDispatchToProps = (dispatch) => ({
  clearAllSelectedDataItems: () => dispatch(clearAllSelectedDataItemsFunction()),
  displaySelectedDataItems: () => dispatch(areRelatedVariablesFilteredFunction())
});

const mapStateToProps = createStructuredSelector({
  selectedDataItems: getDistinctSelectedDataSets()
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
const SelectedDataItemsContainer = compose(withConnect)(SelectedDataItemsComponent);
export default SelectedDataItemsContainer;
