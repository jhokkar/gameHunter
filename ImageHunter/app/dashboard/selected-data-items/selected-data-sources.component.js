/**
 * RelatedVariablesComponent
 */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TooltipComponent from '../../components/tool-tip/tool-tip.component';

class SelectedDataItemsComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.clearAllSelectedDataItemsFunction = this.clearAllSelectedDataItemsFunction.bind(this);
    this.displaySelectedDataItemsFunction = this.displaySelectedDataItemsFunction.bind(this);
  }
  clearAllSelectedDataItemsFunction() {
    this.props.clearAllSelectedDataItems()
  }
  displaySelectedDataItemsFunction(){
    this.props.displaySelectedDataItems()
  }
  render() {
    const { selectedDataItems } = this.props;
    return (
      <div className="d-inline-flex flex-row">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" id="defaultUnchecked" onClick={this.displaySelectedDataItemsFunction}/>
            <label class="custom-control-label" for="defaultUnchecked">Filter Data Sources</label>
        </div>
        <div id="clearDataSources">
          <i
            className="fa fa-custom-close fa-times cursor-pointer"
            onClick={this.clearAllSelectedDataItemsFunction}
          >
          </i>
          <TooltipComponent
            placement="bottom"
            targetId="clearDataSources"
            text="clear dataSources"
          />
        </div>

        <div>
          <span className="badge custom-badge">
            <b>{selectedDataItems ? selectedDataItems.length : '0'}</b>
          </span>
          <cite title="Source Selected"> Data Sources selected</cite>
        </div>
      </div>
    );
  }
}
SelectedDataItemsComponent.propTypes = {
  selectedDataItems: PropTypes.array
};
export default SelectedDataItemsComponent;
