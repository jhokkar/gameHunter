import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { toggleSelectedDataItems, updateIsSelectedDataItemsChanged } from '../dashboard.actions';
import RelatedVariablesComponent from './related-variables.component';
import { getRelatedVariables, getSelectedDataItems, isDataSourceSelectionToggled } from '../dashboard.selector';
import { onRelatedVariableClick } from './related-component.actions';

const mapDispatchToProps = (dispatch) => ({
    toggleSelectedDataItems: (dataItem) => dispatch(toggleSelectedDataItems(dataItem)),
    onRelatedVariableClick: (variableId) => dispatch(onRelatedVariableClick(variableId)),
    updateIsSelectedDataItemsChanged :(value) => dispatch(updateIsSelectedDataItemsChanged(value))

});

const mapStateToProps = createStructuredSelector({
    relatedVariables: getRelatedVariables(),
    selectedDataItems: getSelectedDataItems(),
    isDataSourceSelectionToggled: isDataSourceSelectionToggled()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const RelatedVariablesContainer = compose(withConnect)(RelatedVariablesComponent);
export default RelatedVariablesContainer;
