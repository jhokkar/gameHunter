import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactScrollbar from 'components/react-scroll-bar';

class BadgesBox extends PureComponent {
    constructor(props) {
        super(props);
        this.onShowMore = this.onShowMore.bind(this);
        this.onShowLess = this.onShowLess.bind(this);

        this.state = {
            // list limit
            limit: 4,
            // state of the button
            showLess: false,
            badges: []
        };

    }
    componentDidMount() {
        this.setState({
            badges: [...this.props.badgeList.slice(0, this.state.limit)]
        });
    }


    onShowMore() {
        this.setState({
            // do not show the button any more
            showLess: true,
            // set limit to current count
            // what happens if the comments increase?
            badges: [...this.props.badgeList]
        });
    }
    onShowLess() {
        this.setState({
            // do not show the button any more
            showLess: false,
            // set limit to current count
            // what happens if the comments increase?
            badges: [...this.props.badgeList.slice(0, 4)]
        });
    }


    render() {
        const scrollbar = {
            height: 95,
        };
        // copy up to limit entries from the props.comments
        return (
          <ReactScrollbar style={scrollbar}>
              <div className="badges-info-box">
                  {this.state.badges.map((badge) => (
                      <span key={badge} className="badge custom-badge-info py-1 mr-1">{badge}</span>
                    ))}
                  {
                        (this.props.badgeList.length > this.state.badges.length) ?
                          <span className="cursor-pointer text-theme-color badge custom-badge-info py-1 mr-1" onClick={this.onShowMore}>...more</span> : ''

                    }
                  {this.state.showLess ? <span className="cursor-pointer text-theme-color badge custom-badge-info py-1 mr-1" onClick={this.onShowLess}>...less</span> : ''}

                </div>
            </ReactScrollbar>
        );
    }
}

BadgesBox.propTypes = {
    badgeList: PropTypes.array,
};

export default BadgesBox;