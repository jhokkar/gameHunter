import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { toggleSelectedDataItems, updateIsSelectedDataItemsChanged } from '../dashboard.actions';
import SmartFrameComponent from './smart-frames.component';
import { getSmartFrames } from '../dashboard.selector';

const mapDispatchToProps = (dispatch) => ({
    toggleSelectedDataItems: (dataItem) => dispatch(toggleSelectedDataItems(dataItem)),
    updateIsSelectedDataItemsChanged: (value) => dispatch(updateIsSelectedDataItemsChanged(value))
});

const mapStateToProps = createStructuredSelector({
    smartFrames: getSmartFrames(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const SmartFramesContainer = compose(withConnect)(SmartFrameComponent);
export default SmartFramesContainer;
