import React, { PureComponent } from 'react';
import CheckBoxComponent from 'components/checkbox';
import PropTypes from 'prop-types';
import BadgesBox from './badges-box.component';
import { ReadMore } from 'components/read-more';
import * as _ from 'lodash';

// import StarRatingComponent from 'components/star-rating';

class SmartFrameComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  isSmartFrameChecked(id, dataSource) {
    const isPresent = _.find(this.props.selectedDataItems, (o) => o.id === id && o.dataSource === dataSource);
    return isPresent !== undefined;
  }
  render() {
    return (
      <div className="smart-frame-data">
        <div className="row">

          {this.props.smartFrames.map((smartFrame) => (
            <div key={smartFrame.id} className="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6  p-2">
              <div className="card shadow">
                <div className="row">
                  {/* <div className="col-md-4">
                  <div className="datasource-icon">
                    <img src={Image} alt="SwitchBoardX-Logo" />
                    <div className="text-center">
                      <StarRatingComponent
                        rating={3.4}
                        onChangeRating={(e) => { console.log('start Rating', e); }}
                      />
                    </div>
                  </div>
                </div> */}
                  <div className="col-md-12 px-1 py-1">
                    <div className="card-block px-2">
                      <h5 className="card-title header-font-size py-2">{smartFrame.name}
                        <div className="pull-right">
                          <CheckBoxComponent
                            checked={this.isSmartFrameChecked(smartFrame.id, smartFrame.name)}
                            disabled={false}
                            emitChange={(value) => {
                              this.props.toggleSelectedDataItems({ id: smartFrame.id, dataSource: smartFrame.name, value, type: 'SmartFrame' });
                              this.props.updateIsSelectedDataItemsChanged(true)
                            }}
                          />
                        </div>
                      </h5>

                      <span className="card-title text-muted text-font-size">
                        by <i className="text-theme-color">{smartFrame.createdBy}</i> at <i className="text-theme-color">{smartFrame.createdOn}</i>
                      </span>
                      <div className="card-text text-font-size">
                        <ReadMore
                          isTextNeeded
                          text={smartFrame.description}
                          breakOn="1"
                          charLimit="150"
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="card-block px-1 pb-1">
                      <hr className="hr-text" data-content="Sources" />
                      <BadgesBox badgeList={smartFrame.dataSources} />

                      <hr className="hr-text" data-content="Columns" />
                      <BadgesBox badgeList={smartFrame.columns} />
                    </div>
                  </div>
                </div>
              </div>
            </div>

          ))}
        </div>
      </div>
    );
  }
}

SmartFrameComponent.propTypes = {
  selectedDataItems: PropTypes.array,
  smartFrames: PropTypes.array,
  toggleSelectedDataItems: PropTypes.func
};

export default SmartFrameComponent;
