/*
 * DashboardReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return {...state, 'yourStateVariable': true);
 */
import { updateSearchInput, searchForData, updateSelectedFacets, clearSelectedFacets, searchForFacets, updateSearchFacetsText } from './dashboard.actions';
import {Clear_AVAILABLE_DATASET_NAMES, TOGGLE_SELECTED_DATA_ITEMS, SEARCH_FOR_DATA, UPDATE_SEARCH_INPUT, UPDATE_SELECTED_FACETS, CLEAR_SELECTED_FACETS, UPDATE_AVAILABLE_FACETS, UPDATE_SEARCH_TEXT,CLEAR_ALL_SELECTED_ITEMS,UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG, ARE_RELATED_VARIABLES_FILTERED } from '../constants';
import * as _ from 'lodash';

// The initial state of the App
const initialState = ({
  searchInput: '',
  isDataSourceSelectionToggled: false,
  relatedVariables: [],
  dataSources: [],
  smartFrames: [],
  selectedDataItems: [],
  updateSearchInput,
  searchForData,
  updateSelectedFacets,
  clearSelectedFacets,
  searchForFacets,
  availableFacets: [],
  selectedFacets: [],
  totalInitialFacets: [],
  updateSearchFacetsText,
  facetSearchText: '',
  availableDataSetNames:[],
  isSelectedDataItemsChanged: false,
  areRelatedVariablesFiltered: false
});

function dashboardReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_SEARCH_INPUT: {
      return { ...state, searchInput: action.payload.searchInput };
    }
    case SEARCH_FOR_DATA: {
      return {
        ...state,
        relatedVariables: action.payload.relatedVariables.map((x) => ({ ...x })),
        dataSources: action.payload.dataSources.map((x) => ({ ...x })),
        smartFrames: action.payload.smartFrames.map((x) => ({ ...x })),
        availableFacets: action.payload.availableFacets,
        totalInitialFacets: action.payload.availableFacets,
        selectedFacets: action.payload.selectedFacets,
        availableDataSetNames:action.payload.dataSets
      };
    }
    case UPDATE_SEARCH_TEXT: {
      return {
        ...state,
        facetSearchText: action.payload
      }
    }
    case Clear_AVAILABLE_DATASET_NAMES:{
      return {
        ...state,availableDataSetNames: action.payload.availableDataSetNames.map((x)=>({...x}))
      }
    }
    case UPDATE_SELECTED_FACETS: {
      return {
        ...state, selectedFacets: action.payload.selectedFacets,
        availableFacets: action.payload.availableFacets.map(x => { return { ...x }; })
      };
    }
    case UPDATE_AVAILABLE_FACETS: {
      return {
        ...state, availableFacets: action.payload.map(x => { return { ...x }; })
      }
    }
    case CLEAR_SELECTED_FACETS: {
      return {
        ...state, selectedFacets: [],
        availableFacets: action.payload.availableFacets.map(x => { return { ...x }; })
        
      }
    }
    case CLEAR_ALL_SELECTED_ITEMS: {
      return {
        ...state, selectedDataItems: action.payload.selectedDataItems.map(x => { return { ...x }; })
      }
    }
    case UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG: {
      return {
        ...state, isSelectedDataItemsChanged: action.payload.value
      }
    }
    case ARE_RELATED_VARIABLES_FILTERED:{
      return {
        ...state, areRelatedVariablesFiltered: !state.areRelatedVariablesFiltered
      }
    }
    case TOGGLE_SELECTED_DATA_ITEMS: {
      const currentItem = action.payload;
      let selectedDataSetList = state.selectedDataItems;
      if (currentItem.type === 'Related') {
        const isExistsAlready = _.find(selectedDataSetList,
          (item) => item.ID === currentItem.ID &&
            item.RelatedVarID === currentItem.RelatedVarID &&
            item.type === currentItem.type);

        if (isExistsAlready) {
          selectedDataSetList = _.filter(selectedDataSetList, (item) => !(item.ID === currentItem.ID &&
            item.RelatedVarID === currentItem.RelatedVarID &&
            item.type === currentItem.type));
        } else {
          selectedDataSetList.push(currentItem);
        }
      } else if (currentItem.type === 'DataSet') {
        const isExistsAlready = _.find(selectedDataSetList,
          (item) => item.ID === currentItem.ID &&
            item.type === currentItem.type);

        if (isExistsAlready) {
          selectedDataSetList = _.filter(selectedDataSetList, (item) => !(item.ID === currentItem.ID &&
            item.type === currentItem.type));
        } else {
          selectedDataSetList.push(currentItem);
        }
      }
      const newInstanceOfItems = selectedDataSetList.map((x) => ({ ...x }));
      return { ...state, selectedDataItems: newInstanceOfItems, isDataSourceSelectionToggled: !state.isDataSourceSelectionToggled};
    }
    default:
      return state;
  }
}

export default dashboardReducer;
