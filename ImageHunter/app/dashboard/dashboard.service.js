import { axiosPost } from '../utils/axios-api';

function convertFacetsToDBFormat(selectedFacets) {
  const groupedFilters = {};
  const facetCategories = [];
  _.forEach(selectedFacets, (filter) => {
    if (!groupedFilters[filter.filterCategory]) {
      groupedFilters[filter.filterCategory] = [];
      facetCategories.push(filter.filterCategory);
    }
    groupedFilters[filter.filterCategory].push(filter.filterValue);
  });
  return groupedFilters;
}
//change here
export function getSearchDataBasedOnInput(searchInput, selectedFacets,availableDataSetNames,currentSelectedFacetName,currentSelectedFacetSelectedValues,currentSelectedFacetAllValues,selectedTimeFacets,selectedLocationFacets) {
  let finalFacets = {
    "Whose Data": [],
    "Provider Class": [],
    "Provider Sub-Class": [],
    "Provider": [],
    "Data Source": [],
    "Data Source Class": [],
    "Data Source Sub-Class": [],
    "Data Source Type": [],
    "dataSetName":availableDataSetNames,
    "CurrentSelectionName":currentSelectedFacetName,
    "CurrentSelectionFilters":currentSelectedFacetSelectedValues,
    "CurrentSelectionValues":currentSelectedFacetAllValues
  };
  let finalSelectedTimeDateFacets ={
    "Year":[],
    "Quarter":[],
    "Month":[],
    "Day":[]
  }
  let finalSelectedLocationFacets ={
    "Country":[],
    "States":[],
    "BusinessUnit":[],
    "DMA":[],
    "Counties":[],
    "Cities":[],
    "ZipCodes":[]
  }
  let facets = convertFacetsToDBFormat(selectedFacets);
  if (Object.keys(facets).length > 0) {
    finalFacets = { ...finalFacets, ...facets };
  }
  let timeDateFacets = convertFacetsToDBFormat(selectedTimeFacets);
  if (Object.keys(timeDateFacets).length > 0) {
    finalSelectedTimeDateFacets = { ...finalSelectedTimeDateFacets, ...timeDateFacets };
  }
  let locationFacets = convertFacetsToDBFormat(selectedLocationFacets);
  if (Object.keys(locationFacets).length > 0) {
    finalSelectedLocationFacets = { ...finalSelectedLocationFacets, ...locationFacets };
  }
  const data = {
    data: {
      searchText: searchInput,
      selectedFacets: finalFacets,
      selectedTimeDateFacets: finalSelectedTimeDateFacets,
      selectedLocationFacets: finalSelectedLocationFacets
    }
  };
  return axiosPost('search', data);
}
