import { getSearchDataBasedOnInput } from './dashboard.service';
import { TOGGLE_SELECTED_DATA_ITEMS, CLEAR_ALL_SELECTED_ITEMS, UPDATE_SEARCH_INPUT, SEARCH_FOR_DATA, UPDATE_SELECTED_FACETS, CLEAR_SELECTED_FACETS, UPDATE_AVAILABLE_FACETS, UPDATE_SEARCH_TEXT, Clear_AVAILABLE_DATASET_NAMES, UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG, ARE_RELATED_VARIABLES_FILTERED } from '../constants';
import * as _ from 'lodash';
import { ResponseStatusEnum } from '../constants/enums/response-status-enum';

// #region Search Functionality

export function updateSearchInput(searchInput) {
  return (dispatch) => {
    const payload = {
      searchInput,
    };
    dispatch({
      type: UPDATE_SEARCH_INPUT,
      payload
    });
  };
}
export function clearAvailableDataSetNames() {
  return (dispatch) => {
    const payload = {
      availableDataSetNames: []
    };
    dispatch({
      type: Clear_AVAILABLE_DATASET_NAMES,
      payload
    })
  }
}
export function clearAllSelectedDataItemsFunction() {
  return (dispatch) => {
    const payload = {
      selectedDataItems: []
    };
    dispatch({
      type: CLEAR_ALL_SELECTED_ITEMS,
      payload
    })
  };
}
export function areRelatedVariablesFilteredFunction() {
  return (dispatch) => {
    const payload = {
      isToggled: false
    };
    dispatch({
      type: ARE_RELATED_VARIABLES_FILTERED,
      payload
    })
  };
}

export function searchForData(searchInput, selectedFacets, currentClickedFacetValue) {
  return (dispatch, getState) => {
    const preselectedFacets = getState().dashboard.selectedFacets;
    dispatch(getState().spinnerStore.BeginTask());
    const availableDataSetNames = getState().dashboard.availableDataSetNames;
    let currentSelectedFacetName = "";
    let currentSelectedFacetCategory = "";
    let currentSelectedFacetSelectedValues = [];
    let currentSelectedFacetAllValues = [];
    let currentlySelectedFacetValueFromStore = [];
    _.map(getState().dashboard.availableFacets, (facet) => {
      if (facet.filterValue == currentClickedFacetValue) {
        currentlySelectedFacetValueFromStore.push(facet)
      }
    });
    if (currentlySelectedFacetValueFromStore.length > 0) {
      currentSelectedFacetName = currentlySelectedFacetValueFromStore[0].filterDisplayName;
      currentSelectedFacetCategory = currentlySelectedFacetValueFromStore[0].filterCategory
    }
    let finalCurrentSelectedFacetsSelectedValues = [];
    _.map(selectedFacets, (facet) => {
      if (facet.filterCategory == currentSelectedFacetCategory) { finalCurrentSelectedFacetsSelectedValues.push(facet.filterValue) }
    });
    if (finalCurrentSelectedFacetsSelectedValues.length > 0) {
      currentSelectedFacetSelectedValues = finalCurrentSelectedFacetsSelectedValues;
    }
    const timeDateFacets = ["Year", "Quarter", "Month", "Day"];
    const locationFacets = ["Country", "States", "BusinessUnit", "DMA", "Counties", "Cities", "ZipCodes"]
    // use each instead of filter later
    const finalSelectedTimeFacets = [];
    _.map(selectedFacets, (facet) => {
      if (timeDateFacets.includes(facet.filterCategory)) {
        finalSelectedTimeFacets.push(facet)
      }
    });
    const finalSelectedLocationFacets = [];
    _.map(selectedFacets, (facet) => {
      if (locationFacets.includes(facet.filterCategory)) {
        finalSelectedLocationFacets.push(facet);
      }
    });
    let finalCurrentSelectedFacetAllValues = []
    _.map(getState().dashboard.availableFacets, (facet) => { if (facet.filterCategory == currentSelectedFacetCategory) { finalCurrentSelectedFacetAllValues.push(facet.filterValue) } });
    if (finalCurrentSelectedFacetAllValues) {
      currentSelectedFacetAllValues = finalCurrentSelectedFacetAllValues;
    }
    let selectedFacetsWithoutTimeDateAndLocationFacets = _.difference(selectedFacets, finalSelectedTimeFacets, finalSelectedLocationFacets)
    getSearchDataBasedOnInput(searchInput, selectedFacetsWithoutTimeDateAndLocationFacets, availableDataSetNames, currentSelectedFacetCategory, currentSelectedFacetSelectedValues, currentSelectedFacetAllValues, finalSelectedTimeFacets, finalSelectedLocationFacets).then((response) => {
      dispatch(getState().spinnerStore.EndTask());
      const searchResults = {
        relatedVariables: [],
        facetsList: {},
        dataSources: [],
        dataSets: [],
        timeDateFacets: {},
        locationFacets: {}
      };
      if (response && response.data) {
        if (response.data.relatedVariables) {
          searchResults.relatedVariables = response.data.relatedVariables;
        }
        if (response.data.availableFacets && response.data.availableFacets.length > 0) {
          searchResults.facetsList = response.data.availableFacets[0].result[0];
        }
        if (response.data.dataSources && response.data.dataSources.length > 0) {
          searchResults.dataSources = response.data.dataSources[0].dataSources;
        }
        if (response.data.dataSetName && response.data.dataSetName.length > 0) {
          searchResults.dataSets = response.data.dataSetName;
        }
        if (response.data.timeDateFacets && response.data.timeDateFacets.length > 0) {
          searchResults.timeDateFacets = response.data.timeDateFacets[0];
        }
        if (response.data.locationFacets && response.data.locationFacets.length > 0) {
          searchResults.locationFacets = response.data.locationFacets[0];
        }
        const availableFacets = [];
        try {
          if (searchResults.facetsList && Object.keys(searchResults.facetsList).length > 0) {
            Object.keys(searchResults.facetsList).map((facet) => {
              if (searchResults.facetsList[facet].length > 0) {
                searchResults.facetsList[facet].map((facetValue) => {
                  const foundPreselectedFacet = _.find(preselectedFacets, (x) => (x.filterValue === facetValue && x.filterCategory == facet));
                  let isActive;
                  if (foundPreselectedFacet == undefined) isActive = false;
                  else isActive = true;
                  availableFacets.push({
                    filterDisplayName: facetValue,
                    filterValue: facetValue,
                    isActive,
                    filterCategory: facet,
                  });
                });
              }
            });
          }
          if (searchResults.timeDateFacets && Object.keys(searchResults.timeDateFacets).length > 0) {
            Object.keys(searchResults.timeDateFacets).map((facet) => {
              if (searchResults.timeDateFacets[facet].length > 0) {
                searchResults.timeDateFacets[facet].map((facetValue) => {
                  // Facet Value is an object here ex: "0" : "Q1"
                  let value = Object.keys(facetValue)[0];
                  let displayName = Object.values(facetValue)[0];
                  if (value != -1) {
                    const foundPreselectedFacet = _.find(preselectedFacets, (x) => (x.filterValue === value && x.filterCategory == facet));
                    let isActive;
                    if (foundPreselectedFacet == undefined) isActive = false;
                    else isActive = true;
                    availableFacets.push({
                      filterDisplayName: displayName,
                      filterValue: value,
                      isActive,
                      filterCategory: facet,
                    });
                  }
                });
              }
            });
          }
          if (searchResults.locationFacets && Object.keys(searchResults.locationFacets).length > 0) {
            Object.keys(searchResults.locationFacets).map((facet) => {
              if(searchResults.locationFacets[facet].length > 0){
                searchResults.locationFacets[facet].map((facetValue) => {
                  if (facetValue != -1) {
                    const foundPreselectedFacet = _.find(preselectedFacets, (x) => (x.filterValue === facetValue && x.filterCategory == facet));
                    let isActive;
                    if (foundPreselectedFacet == undefined) isActive = false;
                    else isActive = true;
                    availableFacets.push({
                      filterDisplayName: facetValue,
                      filterValue: facetValue,
                      isActive,
                      filterCategory: facet,
                    });
                  }
                });
              }
            });
          }
        } catch (e) {
          dispatch(getState().notificationStore.notify('Facets Binding failed', ResponseStatusEnum.Error));
          console.log('Facets Binding Error', e);
        }
        let facetOrderDictionary = [
          { "facet": "Whose Data", "displayOrder": 1 },
          { "facet": "Provider Sub-Class", "displayOrder": 2 },
          { "facet": "Data Source", "displayOrder": 3 },
          { "facet": "Provider Class", "displayOrder": 4 },
          { "facet": "Data Source Class", "displayOrder": 5 },
          { "facet": "Data Source Type", "displayOrder": 6 },
          { "facet": "Data Source Sub-Class", "displayOrder": 7 },
          { "facet": "Provider", "displayOrder": 8 },
          { "facet": "Year", "displayOrder": 9 },
          { "facet": "Quarter", "displayOrder": 10 },
          { "facet": "Month", "displayOrder": 11 },
          { "facet": "Day", "displayOrder": 12 },
          { "facet": "Country", "displayOrder": 13 },
          { "facet": "States", "displayOrder": 14 },
          { "facet": "BusinessUnit", "displayOrder": 15 },
          { "facet": "DMA", "displayOrder": 16 },
          { "facet": "Counties", "displayOrder": 17 },
          { "facet": "Cities", "displayOrder": 18 },
          { "facet": "ZipCodes", "displayOrder": 19 },
        ]
        let reOrderedAvailableFacets = reOrderAvailableFacets(availableFacets, facetOrderDictionary);
        const payload = {
          availableFacets: reOrderedAvailableFacets,
          relatedVariables: searchResults.relatedVariables,
          selectedFacets,
          dataSets: searchResults.dataSets,
          dataSources: searchResults.dataSources,
          smartFrames: []
        };
        dispatch(updateSearchInput(searchInput, selectedFacets));
        dispatch(updateSearchFacetsText(''));
        dispatch({
          type: SEARCH_FOR_DATA,
          payload
        });
      }
      // const selectedFacets = resp.data.availableFacets.map(x => {
      //   if (x.isActive) return x.filterName;
      // })
    }).catch((e) => {
      console.log('error', e);
      dispatch(updateSearchInput(searchInput, []));
      dispatch({
        type: SEARCH_FOR_DATA,
        payload: {
          availableFacets: [],
          relatedVariables: [],
          selectedFacets: [],
          dataSources: [],
          smartFrames: []
        }
      });
      dispatch(getState().spinnerStore.EndTask());
      dispatch(getState().notificationStore.notify('Search failed', ResponseStatusEnum.Error));
    });
  };
}
// #endregion Search Functionality

// #region Facets Functionality

function reOrderAvailableFacets(availableFacets, facetDisplayOrderDictionary) {
  if (availableFacets == null) return [];
  if (facetDisplayOrderDictionary == null) return availableFacets;
  if (availableFacets.length === 0 || facetDisplayOrderDictionary.length === 0) return availableFacets;
  let reorderedFacets = [];
  facetDisplayOrderDictionary = facetDisplayOrderDictionary.sort((a, b) => parseInt(a.displayOrder) - parseInt(b.displayOrder));
  facetDisplayOrderDictionary.map((config) => {
    availableFacets.map((facet) => {
      if (facet.filterCategory.toLowerCase() == config.facet.toLowerCase()) {
        reorderedFacets.push(facet);
      }
    });
  });
  const remainingFacets = _.difference(availableFacets, reorderedFacets);
  reorderedFacets.push(...remainingFacets);
  return reorderedFacets;
}

export function updateSearchFacetsText(searchText) {
  return (dispatch) => {
    dispatch({
      type: UPDATE_SEARCH_TEXT,
      payload: searchText
    });
  };
}
export function updateIsSelectedDataItemsChanged(value){
  return (dispatch) => {
    dispatch({
      type: UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG,
      payload:{"value":value}
    })
  }
}
// Update Search Here
export function clearSelectedFacets() {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());
    const availableFacets = getState().dashboard.availableFacets || [];
    dispatch({
      type: CLEAR_SELECTED_FACETS,
      payload: {
        availableFacets
      }
    });
    dispatch(updateSearchFacetsText(''));
    dispatch(getState().spinnerStore.EndTask());
  };
}


export function searchForFacets(searchText) {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());
    const totalInitialFacets = getState().dashboard.totalInitialFacets || [];
    let filteredAvailableFacets = [];
    if (searchText === '') {
      filteredAvailableFacets = totalInitialFacets;
    } else {
      totalInitialFacets.map((facet) => {
        if (facet.filterDisplayName.toLowerCase().includes(searchText.toLowerCase()) || facet.filterCategory.toLowerCase().includes(searchText.toLowerCase())) {
          filteredAvailableFacets.push(facet);
        }
      });
    }
    dispatch(updateSearchFacetsText(searchText));
    dispatch({
      type: UPDATE_AVAILABLE_FACETS,
      payload: filteredAvailableFacets
    });
    dispatch(getState().spinnerStore.EndTask());
  };
}
// change here
export function updateSelectedFacets(toggledFacet) {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());
    const totalSelectedFacets = getState().dashboard.selectedFacets || [];
    const availableFacets = getState().dashboard.availableFacets || [];

    const isExistingFacet = _.find(totalSelectedFacets, (x) => (x.filterValue === toggledFacet.filterValue));
    // adding or removing the toggled facet from selectedFacets array
    if (isExistingFacet && isExistingFacet.filterValue) {
      const index = _.findIndex(totalSelectedFacets, (x) => x.filterValue === toggledFacet.filterValue);
      if (index > -1) {
        totalSelectedFacets.splice(index, 1);
      }
    } else {
      totalSelectedFacets.push(toggledFacet);
    }
    // needs refactoring
    // changing active and inactive status of available facets based on selectedFacets array to re-render the search-filter container
    availableFacets.map((availableFacet) => {
      const isActive = _.find(totalSelectedFacets, (x) => x.filterValue == availableFacet.filterValue);
      if (isActive) {
        availableFacet.isActive = true;
      } else {
        availableFacet.isActive = false;
      }
    });

    const payloadVariable = {
      selectedFacets: totalSelectedFacets,
      availableFacets
    };

    dispatch({
      type: UPDATE_SELECTED_FACETS,
      payload: payloadVariable
    });
    // dispatch(searchForData(getState().dashboard.searchInput, totalSelectedFacets, toggledFacet.filterValue));
    dispatch(getState().spinnerStore.EndTask());
  };
}

// #endregion Facets Functionality

// #region Data source selection Functionality

export function toggleSelectedDataItems(selectedDataSource) {
  return (dispatch) => {
    dispatch({
      type: TOGGLE_SELECTED_DATA_ITEMS,
      payload: selectedDataSource
    }
    );
  };
}

// #endregion
