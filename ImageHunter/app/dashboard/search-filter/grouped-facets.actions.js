
export function onSelectionOfEachFacet(toggledFacet) {
    return (dispatch, getState) => {
        const updateSelectedFacetsMethod = getState().dashboard.updateSelectedFacets;
        dispatch(updateSelectedFacetsMethod(toggledFacet));
        let selectedFacets = getState().dashboard.selectedFacets;
        const searchForDataMethod = getState().dashboard.searchForData;
        const searchInput = getState().dashboard.searchInput;
        dispatch(searchForDataMethod(searchInput, selectedFacets, toggledFacet.filterValue))
    };
};
