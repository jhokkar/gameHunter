import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import GroupedFacetsComponent from './grouped-facets.component';
import { onSelectionOfEachFacet } from './grouped-facets.actions';

const mapDispatchToProps = (dispatch) => ({
    updateDataBasedOnFacets: (toggledFacet) => dispatch(onSelectionOfEachFacet(toggledFacet))
});

const mapStateToProps = createStructuredSelector({
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const GroupedFacetsComponentContainer = compose(withConnect)(GroupedFacetsComponent);
export default GroupedFacetsComponentContainer;
