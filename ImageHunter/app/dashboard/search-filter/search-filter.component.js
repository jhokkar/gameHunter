import React, { PureComponent } from 'react';
import GroupedFacetedFilterComponent from './grouped-facets.container';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import { Scrollbars } from 'react-custom-scrollbars';

class SearchFilterComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.resetFacets = this.resetFacets.bind(this);
    this.searchForFacets = this.searchForFacets.bind(this);
    this.onSearchInputChange = this.onSearchInputChange.bind(this);
    this.toggleMoreLessSearch = this.toggleMoreLessSearch.bind(this);
    this.state = {
      isToggled: false
    }
  }
  groupedFilters = [];
  facetCategories = [];
  groupFacetsByCategory() {
    this.groupedFilters = [];
    this.facetCategories = [];
    if (!this.props.facetedFilters) return;
    _.forEach(this.props.facetedFilters, (filter) => {
      if (!this.groupedFilters[filter.filterCategory]) {
        this.groupedFilters[filter.filterCategory] = [];
        this.facetCategories.push(filter.filterCategory);
      }
      this.groupedFilters[filter.filterCategory].push(filter);
    });
  }
  resetFacets() {
    this.props.resetFacets();
  }
  searchForFacets(event) {
    const searchText = event.target.value;
    this.props.searchForFacet(searchText);
  }
  onSearchInputChange(event) {
    const searchText = event.target.value;
    this.props.updateFacetSearchText(searchText);
  }
  toggleMoreLessSearch() {
    console.log('toggleMoreLess');
    this.setState({ isToggled: !this.state.isToggled });
  }
  renderTrackVerticalDefault({ style, ...props }) {
    const finalStyle = {
        ...style,
        right: 2,
        bottom: 2,
        top: 2,
        borderRadius: 3,
        width:'12px'

    };
    return <div style={finalStyle} {...props} />;
}
  renderThumbVerticalDefault({ style, ...props }) {
    const finalStyle = {
        ...style,
        cursor: 'pointer',
        borderRadius: 'inherit',
        backgroundColor: 'rgba(34,195,212,.5)',
        width:'12px'
    };
    return <div style={finalStyle} {...props} />;
}
  render() {
    this.groupFacetsByCategory();
    return (
      <Scrollbars 
        renderTrackVertical={this.renderTrackVerticalDefault}
        renderThumbVertical={this.renderThumbVerticalDefault}
        className="col-md-3 col-sm-12 col-xs-12 mr-1 filter-sidebar react-scrollbar-default">
        <div className="search-filter py-1">
          <ul className="list-inline search-filter-bar">
            <li className="list-inline-item">
              <h5 className="opacity-7">Filters</h5>
            </li>
            <li className="list-inline-item pull-right">
              <a color="primary" href="javascript:void(0)" style={{ marginBottom: '1rem' }} onClick={() => this.resetFacets()}>
                <i className="fa fa-refresh"></i> Reset
            </a>
            </li>
            <li>
              <input
                className="form-control height-30"
                type="text"
                placeholder="Search for filter"
                value={this.props.facetSearchInput}
                onChange={(event) => this.onSearchInputChange(event)}
                onKeyUp={(event) => this.searchForFacets(event)}
              />
            </li>
          </ul>
          {this.facetCategories.map((category, index) => (
            <GroupedFacetedFilterComponent key={index} toggleMoreLessSearch={this.toggleMoreLessSearch} filterCategory={category} filters={this.groupedFilters[category]} />
          ))}
          {this.facetCategories.length === 0 ? <div className="text-center p-2">No Facets Found</div> : ''}
        </div>
      </Scrollbars>

    );
  }
}

SearchFilterComponent.propTypes = {
  facetedFilters: PropTypes.array,
  facetSearchInput: PropTypes.string
};
export default SearchFilterComponent;
