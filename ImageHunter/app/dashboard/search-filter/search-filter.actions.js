import {clearAvailableDataSetNames} from '../dashboard.actions';

export function clearSelectedFacets(){
    return (dispatch, getState) => {
        const clearSelectedFacetsMethod = getState().dashboard.clearSelectedFacets;
        const searchForDataMethod = getState().dashboard.searchForData;
        const presentSearchText = getState().dashboard.searchInput;
        dispatch(clearAvailableDataSetNames());
        dispatch(clearSelectedFacetsMethod());
        dispatch(searchForDataMethod(presentSearchText,[],''));
    };
}

export function searchForFacets(searchText){
    return(dispatch,getState) => {
        const searchForFacetsMethod = getState().dashboard.searchForFacets ;
        dispatch(searchForFacetsMethod(searchText));
    }
}

export function updateFacetSearchInput(searchInput) {
    return(dispatch,getState) => {
        const updateSearchFacetsTextMethod= getState().dashboard.updateSearchFacetsText ;
        dispatch(updateSearchFacetsTextMethod(searchInput));
    }
  }
  