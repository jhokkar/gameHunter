import React, { PureComponent } from 'react';
import { Collapse } from 'reactstrap';
import Facet from 'components/facet';
import PropTypes from 'prop-types';

const MAX_ITEMS = 7;

class GroupedFacetsComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.toggleCategory = this.toggleCategory.bind(this);
    this.state = { collapse: true, isOpen: false };
    this.toggleFacet = this.toggleFacet.bind(this);
  }
  getRenderedItems() {
    if (this.state.isOpen) {
      return this.props.filters;
    }
    return this.props.filters.slice(0, MAX_ITEMS);
  }
  toggleMoreLess = () => {
    this.setState({ isOpen: !this.state.isOpen });
    this.props.toggleMoreLessSearch();
    let scrollElement = document.getElementById(this.props.filterCategory.replace(/ /g,''));
    scrollElement.scrollIntoView();
  }

  toggleCategory() {
    this.setState({ collapse: !this.state.collapse });
  }
  toggleFacet(index, category) {
    this.props.updateDataBasedOnFacets({
      filterValue: this.props.filters[index].filterValue,
      filterCategory: category
    }
    );
  }
  render() {
    return (
      <div>
        <div id={this.props.filterCategory.replace(/ /g,'')} className="faceted-header">
          <ul className="list-inline py-1">
            <li className="list-inline-item">
              <p >{this.props.filterCategory}</p>
            </li>
            <li className="list-inline-item pull-right">
              <a color="primary" href='javascript:void(0)' onClick={this.toggleCategory} style={{ marginBottom: '1rem' }}>
                <i className={this.state.collapse ? 'fa fa-angle-up' : 'fa fa-angle-down'}></i>
              </a>
            </li>
          </ul>
        </div>
        <Collapse isOpen={this.state.collapse}>
          <ul className={`list-inline facet-list ${this.props.filterCategory.replace(' ', '')}`}>
            {this.getRenderedItems().map((filter, index) => (
              <li key={index} className="list-inline-item" >
                <Facet facet={filter} toggleFacet={() => this.toggleFacet(index, this.props.filterCategory)} />
              </li>
            ))}
            {this.props.filters.length > MAX_ITEMS ?
              <span className="cursor-pointer text-theme-color badge custom-badge-info" onClick={this.toggleMoreLess}>
                {this.state.isOpen ? '..less' : '..more'}
              </span> : ''

            }

          </ul>
        </Collapse>
      </div>
    );
  }
}


GroupedFacetsComponent.propTypes = {
  filterCategory: PropTypes.string,
  updateDataBasedOnFacets: PropTypes.func,
  filters: PropTypes.array
};

export default GroupedFacetsComponent;
