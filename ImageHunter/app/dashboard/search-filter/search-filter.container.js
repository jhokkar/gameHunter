import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import SearchFilterComponent from './search-filter.component';
import { getAvailableFacets, getFacetSearchInput } from '../dashboard.selector';
import { clearSelectedFacets, searchForFacets, updateFacetSearchInput} from './search-filter.actions';

const mapDispatchToProps = (dispatch) => ({
    resetFacets : () => dispatch(clearSelectedFacets()),
    searchForFacet : (searchText) => dispatch(searchForFacets(searchText)),
    updateFacetSearchText: (searchText) => dispatch(updateFacetSearchInput(searchText))
});

const mapStateToProps = createStructuredSelector({
    facetedFilters: getAvailableFacets(),
    facetSearchInput: getFacetSearchInput()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const SearchFilterComponentContainer = compose(withConnect)(SearchFilterComponent);
export default SearchFilterComponentContainer;
