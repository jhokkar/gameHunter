/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import React from 'react';

import './not-found.scss';

export default function NotFound() {
  return (
    <section className="not-found-page">
      <div className="middle-box text-center animated fadeInDown">
        <h1>Error</h1>
        <h3 className="font-bold">Page Not Found</h3>

        <div className="error-desc">
          Sorry, but the page you are looking for has note been found. Try checking the URL for error, then hit the refresh button
          on your browser or try found something else in our app.
        </div>
      </div>
    </section>
  );
}
