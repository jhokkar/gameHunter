/*
 * Home Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './game.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Webcam from 'react-webcam';
import { checkImageForAMatch, takeImage, loadPreTrainedDataToKNNFromServer, loadPreTrainedImagesToKNNFromServer, getGameItemsFromServer, getGameItemsWithImagesFromServer, predictImage, createKnnImageClassifier } from './game.service'
import Logo from './WIN_20180810_22_43_16_Pro.jpg';
import Countdown from 'react-countdown-now';

class GameComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.stopGameFunction = this.stopGameFunction.bind(this);
        this.renderGame = this.renderGame.bind(this);
    }
    remainingTime = 60;
    stopGame = false;
    currentItemFound = false;
    currentItem = {};
    gameItems = []
    componentDidMount() {
        let trainedDataFromServer = []
        // let gameItems = []
        // getGameItemsFromServer().then((res) => {
        this.props.isTrainingComplete(false);
        getGameItemsWithImagesFromServer().then((res) => {
            trainedDataFromServer = res.data;
            let numberOfItemClasses = trainedDataFromServer.length;
            createKnnImageClassifier(numberOfItemClasses, 10).then(async () => {
                this.gameItems = await loadPreTrainedImagesToKNNFromServer(trainedDataFromServer);
                this.props.setGameItems(this.gameItems);
                this.props.isTrainingComplete(true);
                this.startGame();
            });
        })
    }
    render() {
        return (
            <GameComponent />
        );
    }
}



GameComponent.propTypes = {

};


export default GameComponent;
