
export function setGameItems(gameItems) {
    return (dispatch, getState) => {

      let remainingGameItems = gameItems;
      if(getState().game.gamePlayCount > 1){
        remainingGameItems = getState().game.notFoundItems;
      }else{
        _.map(gameItems, (gameItem)=>{
          gameItem.notFoundCount = 0;
        })
        console.log(gameItems);
      }

      const shuffledGameItems =  _.shuffle(remainingGameItems);
      const payload = {
        gameItems: gameItems,
        remainingGameItems: shuffledGameItems
      };
      dispatch({
        type: 'SET_GAME_ITEMS',
        payload
      });
    };
  }
export function getGameItems(){
    return (dispatch, getState) => {
        return getState().game.gameItems
    }
}
export function calculateScoreForItem(initialScoreForItem , timeTaken){
    let remainingTime = ( 60 - timeTaken );
    let score = 0;
    if (remainingTime < 10 ) score = 5;
    else if(remainingTime> 10 && remainingTime < 20) score = 10;
    else if(remainingTime> 20 && remainingTime < 30) score = 15;
    else if(remainingTime > 30 && remainingTime < 40) score = 20;
    else if(remainingTime > 40 && remainingTime < 50) score = 25;
    else if(remainingTime > 50 ) score = (35 - timeTaken);

    return initialScoreForItem + score ;
}
export function gameItemDiscovered(gameItem,timeTaken) {
    return (dispatch, getState) => {
        // debugger;
        let foundItem = {
            "gameItem":gameItem,
            "timeTaken":timeTaken
        }
        let alreadyFoundItems = getState().game.gameItemsFound ;
        let remainingGameItems = getState().game.remainingGameItems ;
        let initialScore = getState().game.finalScore;
        alreadyFoundItems.push(foundItem);
        _.remove(remainingGameItems , (item) => {
            item.item == gameItem.item
        })
        const finalScore = calculateScoreForItem(initialScore, timeTaken)
        const payload = {
            "alreadyFoundItems" : alreadyFoundItems,
            "remainingGameItems": remainingGameItems,
            "finalScore" : finalScore
        }
        dispatch({
            type: "GAME_ITEM_DISCOVERED",
            payload
        });
    }
}

export function gameItemNotFound() {
    return (dispatch, getState) => {
      // debugger;
      const remainingGameItems = getState().game.remainingGameItems ;
      const notFoundItems = getState().game.gameItemsNotFound ;
      const currentGameItem = remainingGameItems[0];
      currentGameItem.notFoundCount += 1;

      //Todo update not found count here
      //check if a countitem exists


      remainingGameItems.shift();
      notFoundItems.push(currentGameItem);

      const payload = {
        notFoundItems : notFoundItems,
        remainingGameItems: remainingGameItems,
      }

      dispatch({
        type: "GAME_ITEM_NOT_FOUND",
        payload
      });
    }
  }

export function getGameItemsNotFound() {
    return (dispatch, getState) => {
        return getState().game.gameItemsNotFound
    }
}

export function getGamePlayCount() {
    return (dispatch, getState) => {
        return getState().game.gamePlayCount
    }
}

export function increaseGamePlayCount() {
    let gamePlayCount = getState().game.gamePlayCount;
    const payload = {
        "gamePlayCount" : gamePlayCount + 1
        dispatch({
            type: "GAME_PLAY_COUNT",
            payload
        });
    }

}

export function isTrainingComplete(trainingComplete){
    debugger;
    return (dispatch, getState) => {
        const payload = {
            "trainingComplete" : trainingComplete
        }
        console.log("training complete: ",trainingComplete);
        dispatch({
            type: "TRAINING_COMPLETE",
            payload
        });
    }
}
export function isGameFinished(){
    return (dispatch, getState) => {
        if(getState().game.remainingGameItems.length == 0){
            return true;
        }
        else return false;
    }
}
export function getNextGameItem(){
    // debugger;
    return (dispatch, getState) => {
        if( getState().game.remainingGameItems.length >= 0){
            let nextGameItem = getState().game.remainingGameItems[0];
            return nextGameItem;
        }
        else return null;
    }
}

export function reduceTimerBySeconds(time){
    return (dispatch, getState)=>{
      let remainingTime = getState().game.remainingTime;
      remainingTime -= time;
      const payload = {
        remainingTime : remainingTime
      }
      dispatch({
        type: 'REDUCE_REMAINING_TIME',
        payload
      });
    };
  }


export function setImageLastCaptured(timeOfCapture){
    return (dispatch, getState)=>{
      const payload = {
        imageLastCaptured : timeOfCapture
      }
      dispatch({
        type: 'SET_IMAGE_LAST_CAPTURED',
        payload
      });
    };
  }
