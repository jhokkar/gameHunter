import React, { PureComponent } from 'react';

class GameViewComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.timeInSeconds = this.stopGameFunction.bind(this);
        this.renderGame = this.renderGame.bind(this);
    }