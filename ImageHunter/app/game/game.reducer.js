
const initialState = ({
    score: {},
    gameItems: [],
    gameItemsFound: [],
    gameItemsNotFound: [],
    remainingGameItems: [],
    finalScore: 0,
    isCameraRunning: false,
    // remainingTime,
    isGameFinished: false,
    trainingComplete: false,
    gamePlayCount: 0,
    imageLastCaptured: new Date(),
    remainingTime: 60
});

function gameReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_GAME_ITEMS":
            return { ...state, gameItems: action.payload.gameItems, remainingGameItems: action.payload.remainingGameItems };
        case "GAME_ITEM_DISCOVERED":
            return { ...state, remainingGameItems: action.payload.remainingGameItems.map((x) => ({ ...x })), gameItemsFound: action.payload.alreadyFoundItems.map((x) => ({ ...x })), finalScore: action.payload.finalScore };
        case "GAME_ITEM_NOT_FOUND":
            return {
                ...state, remainingGameItems: action.payload.remainingGameItems.map((x) => ({ ...x })),
                gameItemsNotFound: action.payload.notFoundItems.map((x) => ({ ...x }))
            }
        case "TRAINING_COMPLETE":
            return {
                ...state, trainingComplete: action.payload.trainingComplete
            }
        case 'REDUCE_REMAINING_TIME':
            return {
                ...state, remainingTime: action.payload.remainingTime
            };
        case 'SET_IMAGE_LAST_CAPTURED':
            return {
                ...state, imageLastCaptured: action.payload.imageLastCaptured
            };
        case 'GAME_PLAY_COUNT':
            return {
                ...state, gamePlayCount: action.payload.gamePlayCount
            }
        default:
            return state;
    }
}

export default gameReducer;
