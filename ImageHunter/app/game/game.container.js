import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import reducer from './game.reducer';
import { setGameItems ,getGameItems, gameItemDiscovered, getNextGameItem, isGameFinished, gameItemNotFound, isTrainingComplete, reduceTimerBySeconds, setImageLastCaptured} from './game.actions';
import { getIsTrainingCompletedFlag ,getRemainingTime, getImageLastCaptured } from './game.selector';
import GameComponent from './game.component';

const mapDispatchToProps = (dispatch) => ({
    setGameItems: (gameItems) => dispatch(setGameItems(gameItems)),
    getGameItems: () => dispatch(getGameItems()),
    foundGameItem : (gameItem,timeTaken) => dispatch(gameItemDiscovered(gameItem,timeTaken)),
    gameItemNotFound: () => dispatch(gameItemNotFound()),
    getGameItemNotFound: () => dispatch(getGameItemNotFound()),
    getGamePlayCount: () => dispatch(getGamePlayCount()),
    increaseGamePlayCount: () => dispatch(increaseGamePlayCount())
    getNextGameItem : () => dispatch(getNextGameItem()),
    isGameFinished : () => dispatch(isGameFinished()),
    isTrainingComplete : (trainingComplete) => dispatch(isTrainingComplete(trainingComplete)),
    reduceTimerBySeconds: (time)=> dispatch(reduceTimerBySeconds(time)),
    setImageLastCaptured: (timeOfCapture) => dispatch(setImageLastCaptured(timeOfCapture))
});

const mapStateToProps = createStructuredSelector({
    // publishedSmartFrames: getPublishedSmartFrames()
    isTrainingCompletedFlag: getIsTrainingCompletedFlag(),
    remainingTime : getRemainingTime(),
    imageLastCaptured : getImageLastCaptured()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'game', reducer });

const GameContainer = compose(withReducer, withConnect)(GameComponent);
export default GameContainer;
