/**
 * Home selectors
 */

import { createSelector } from 'reselect';

const IsTrainingCompletedFlagSelector = (state) => (state.game ? state.game.trainingComplete : false);

const getIsTrainingCompletedFlag = () => createSelector(
    IsTrainingCompletedFlagSelector,
    (isTrainingComplete) => { return isTrainingComplete }
);

const remainingTime = (state) => (state.game ? state.game.remainingTime : 0);

const getRemainingTime = () => createSelector(
    remainingTime,
    (remainingTime) => { return remainingTime }
);
const imageLastCaptured = (state) => (state.game ? state.game.imageLastCaptured : new Date());

const getImageLastCaptured = () => createSelector(
    imageLastCaptured,
    (imageLastCaptured) => { return imageLastCaptured }
);  

export {
    getIsTrainingCompletedFlag,
    getRemainingTime,
    getImageLastCaptured
};
