/*
 * Home Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './game.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as _ from 'lodash';
import Webcam from 'react-webcam';
import { checkImageForAMatch, takeImage, loadPreTrainedDataToKNNFromServer, loadPreTrainedImagesToKNNFromServer, getGameItemsFromServer, getGameItemsWithImagesFromServer, predictImage, createKnnImageClassifier } from './game.service'
import Logo from './WIN_20180810_22_43_16_Pro.jpg';
import Countdown from 'react-countdown-now';

class GameComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
    constructor(props) {
        super(props);
        this.stopGameFunction = this.stopGameFunction.bind(this);
        // this.renderGame = this.renderGame.bind(this);
    }
    remainingTime = 60;
    stopGame = false;
    currentItemFound = false;
    currentItem = {};
    gameItems = []
    componentDidMount() {
        let trainedDataFromServer = []
        // let gameItems = []
        // getGameItemsFromServer().then((res) => {
        this.props.isTrainingComplete(false);
        getGameItemsWithImagesFromServer().then((res) => {
            trainedDataFromServer = res.data;
            let numberOfItemClasses = trainedDataFromServer.length;
            createKnnImageClassifier(numberOfItemClasses, 10).then(async () => {
                this.gameItems = await loadPreTrainedImagesToKNNFromServer(trainedDataFromServer);
                console.log("training Complete")
                this.props.setGameItems(this.gameItems);
                this.props.isTrainingComplete(true);
                this.startGame();
            });
        })
    }
    async startGame() {
        this.props.increaseGamePlayCount();
        this.loadNextItemFromStore();
        // startTimer()
        await this.keepGameRunning();
    }
    itemFound = () => {
        console.log("found before timer went down");
        this.currentItemFound = true;
        let timeTaken = 60 - this.remainingTime;
        this.setTimer(0);
        this.props.foundGameItem(this.currentItem, timeTaken);
        // ResetTimer()
    }
    drawImage(src) {
        // debugger
        var x = document.createElement("IMG");
        x.setAttribute("src", src);
        x.setAttribute("width", "227");
        x.setAttribute("height", "227");
        x.setAttribute("alt", "couldntLoad");
        document.body.appendChild(x);
    }
    itemNotFound = () => {
        console.log("not found after timer went down");
        this.props.gameItemNotFound();
    }
    loadNextItemFromStore = () => {
        let currentItem = this.props.getNextGameItem();
        if(currentItem){
            this.currentItem = currentItem;
            this.setTimer(60);
            this.currentItemFound = false;
        }
    }
    setTimer = (time) => {
        this.remainingTime = time;
    }


    // runGame = () => {
    //     this.reduceTimerBySeconds(0.5);
    //     if (this.remainingTime > 0 && this.currentItemFound == false && this.stopGame == false) {
    //         this.searchForCurrentItem().then((res) => {
    //             if (res.classIndex == this.currentItem.level && res.confidences[0] >= 0.8) {
    //                 this.itemFound();
    //             }

    //         }
    //     }
    //     else return;
    // }




    async keepGameRunning() {
        console.log("Game Starting");
        let i = 1;
        while (this.props.remainingTime > 0 && this.currentItemFound == false && this.stopGame == false && this.currentItem != null) {
            // keep calling check for item
            var difference = new Date() - this.props.imageLastCaptured;
            if(difference > 500) {
                try {
                    var res = await this.searchForCurrentItem();
                    // debugger;
                    console.log(res.confidences);
                    console.log(this.gameItems[res.classIndex]);
                    if (res.classIndex == this.currentItem.level && res.confidences[0] >= 0.8) {
                        this.itemFound();
                    }
                    this.props.setImageLastCaptured(new Date())	;
                } catch (ex) {
                    //console.log(ex)
                }
            }

        }
        if (!this.currentItemFound) this.itemNotFound();
        if (this.props.isGameFinished() == false) {
            this.loadNextItemFromStore();
            await this.keepGameRunning();
        }
        else return; // write what happens after game finishes.
    }

    async restartGame() {
        console.log("game restarting");
        let gamePlayCount = this.props.getGamePlayCount();
        let gameItemsNotFound = this.props.gameItemsNotFound();
        let newGameItems = gameItemsNotFound.filter(item => item.notFoundCount == gamePlayCount);
        this.props.setGameItems(newGameItems);
        this.startGame();
    }

    async finishGame() {

    }

    searchForCurrentItem() {
        let currentItem = this.currentItem;
        let video = this.webcam.video;
        // debugger;
        let staticImage = document.getElementById("image");
        let image = takeImage(video);
        // this.reduceTimerBySeconds(0.5);
        return predictImage(image);
        // predictImage(image).then((res) => {
        //     console.log("hit");
        //     console.log(res);
        //     if (res.classIndex == this.currentItem.level && res.confidences[0] >= 0.8) {
        //         this.itemFound();
        //     }
        // });
    }
    reduceTimerBySeconds = (seconds) => {
        this.remainingTime = this.remainingTime - seconds;
    }
    stopGameFunction = () => {
        this.stopGame = true
    }

    setRef = (webcam) => {
        this.webcam = webcam;
    }
    // renderGame = () => {
    //     const videoConstraints = {
    //         width: 1280,
    //         height: 720,
    //         facingMode: 'environment',
    //     };
    //     return (
    //         <div>
    //             <div className="timer">Remaining Time : {this.remainingTime} </div>
    //             <div className="game-camera-screen">
    //                 <Webcam ref={this.setRef} audio={false} height={227} width={227} screenshotFormat="image/jpeg" screenshotQuality={1} videoConstraints={videoConstraints} />
    //                 <Countdown date={Date.now() + 60000} onTick={() => {
    //                     this.reduceTimerBySeconds(1);
    //                     console.log(this.remainingTime);
    //                 }} />
    //             </div>
    //             {/* <img id={"image"} src={Logo} height={227} width={227} /> */}

    //             <button onClick={this.stopGameFunction}>End Game</button>
    //         </div>
    //     );
    // }
    render() {
        const videoConstraints = {
            width: 1280,
            height: 720,
            facingMode: 'environment',
        };
        const isTrainingCompleted = this.props.isTrainingCompletedFlag;
        console.log("called render");
        debugger;
        let gameComp = "";

        return (
            <section>
                <Helmet>
                    <title>Game</title>
                    <meta name="description" content="Game" />
                </Helmet>
                <div className="landing-page">
                    <div className="limiter">
                       {/* {this.props.isTrainingCompletedFlag ?   */}
                        <div>
                            <div className="timer">Remaining Time : {this.remainingTime} </div>
                            <div className="game-camera-screen">
                                <Webcam ref={this.setRef} audio={false} height={227} width={227} screenshotFormat="image/jpeg" screenshotQuality={1} videoConstraints={videoConstraints} />
                                <Countdown date={Date.now() + 60000} onTick={() => {
                                    this.props.reduceTimerBySeconds(1);
                                }} />
                            </div>
                            {/* <img id={"image"} src={Logo} height={227} width={227} /> */}

                            <button onClick={this.stopGameFunction}>End Game</button>
                        </div>

                    </div>
                </div>
            </section>
        );
    }
}



GameComponent.propTypes = {

};


export default GameComponent;
