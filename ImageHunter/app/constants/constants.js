/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const LOAD_REPOS = 'App/LOAD_REPOS';
export const LOAD_REPOS_SUCCESS = 'App/LOAD_REPOS_SUCCESS';
export const LOAD_REPOS_ERROR = 'App/LOAD_REPOS_ERROR';
export const DEFAULT_LOCALE = 'en';

export const UPDATE_SEARCH_INPUT = 'header/UPDATE_SEARCH_INPUT';
export const SEARCH_FOR_DATA = 'header/SEARCH_FOR_DATA';
export const BIND_PUBLISHED_SMARTFRAMES = 'home/BIND_PUBLISHED_SMARTFRAMES';

export const TOGGLE_SELECTED_DATA_ITEMS = 'dashboard/TOGGLE_SELECTED_DATA_ITEMS';

export const ADD_UPDATE_SELECTED_COLUMNS = 'smartFrame/ADD_UPDATE_SELECTED_COLUMNS';
export const ON_DATA_SOURCE_SELECTION = 'smartFrame/ON_DATA_SOURCE_SELECTION';
export const ON_VARIABLE_SELECTION = 'smartFrame/ON_VARIABLE_SELECTION';
export const BIND_PUBLISH_SMART_FRAME_POPUP = 'smartFrame/BIND_PUBLISH_SMART_FRAME_POPUP';

export const BIND_SMART_FRAME_DATA = 'smartFrameData/BIND_SMART_FRAME_DATA';
export const BIND_PUBLISH_SMARTFRAME_POPUP_ONE = 'smartFrameData/BIND_PUBLISH_SMARTFRAME_POPUP_ONE';

export const UPDATE_SELECTED_FACETS = 'search-filter/UPDATE_SELECTED_FACETS';
export const UPDATE_HEADER_SEARCH_INPUT = 'header/UPDATE_HEADER_SEARCH_INPUT';
export const BIND_DATA_SOURCE_RESULTS = 'datasource-details/BIND_DATA_SOURCE_RESULTS';
export const BIND_DATA_SOURCE_PREVIEW_DATA = 'datasource-details/BIND_DATA_SOURCE_PREVIEW_DATA';

export const BIND_RELATED_VAR_RESULTS = 'related-var-details/BIND_RELATED_VAR_RESULTS';
export const BIND_RELATED_VAR_PREVIEW_DATA = 'related-var-details/BIND_RELATED_VAR_PREVIEW_DATA';
export const CLEAR_SELECTED_FACETS = 'dashboard/CLEAR_SELECTED_FACETS';
export const UPDATE_AVAILABLE_FACETS = 'dashboard/UPDATE_AVAILABLE_FACETS';
export const UPDATE_SEARCH_TEXT = 'dashboard/UPDATE_SEARCH_TEXT';
export const CLEAR_ALL_SELECTED_ITEMS = 'dashboard/CLEAR_ALL_SELECTED_ITEMS';
export const Clear_AVAILABLE_DATASET_NAMES = 'dashboard/Clear_AVAILABLE_DATASET_NAMES';
export const UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG = 'dashboard/UPDATE_SELECTED_DATA_ITEMS_CHANGED_FLAG';
export const ARE_RELATED_VARIABLES_FILTERED = 'dashboard/ARE_RELATED_VARIABLES_FILTERED';
