/*
 * Datasource Details Reducer
 */
import { BIND_DATA_SOURCE_RESULTS,BIND_DATA_SOURCE_PREVIEW_DATA } from '../constants';

const initialState = ({
    selectedDataSourceId: 0,
    dataSourceDetails: {
        RelatedVariables: []
    },
    previewData: []
});

function dataSourceDetailsReducer(state = initialState, action) {
    switch (action.type) {
        case BIND_DATA_SOURCE_RESULTS: {
            return { ...state, selectedDataSourceName: action.payload.selectedDataSourceName, dataSourceDetails: action.payload.dataSourceDetails };
        }
        case BIND_DATA_SOURCE_PREVIEW_DATA: {
            return { ...state, previewData: [...action.payload.previewData] };
        }
        default:
            return state;
    }
}

export default dataSourceDetailsReducer;
