import React, { PureComponent } from 'react';
import TableComponent from 'components/table/table';

class PreviewDataComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="row mt-2 p-2">
        <div className="datasource-preview-data col-12">
          <h5 className="p-2">Preview Data</h5>
          {this.props.data && this.props.data.length > 0 ?
            <TableComponent tableData={this.props.data}></TableComponent>
            :
            'No Records Found'
          }
        </div>
      </div >
    );
  }
}
export default PreviewDataComponent;
