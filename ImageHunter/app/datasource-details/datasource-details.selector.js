/**
 * DashBoard selectors
 */

import { createSelector } from 'reselect';

const selectedDataSourceSelector = (state) => (state.dataSourceDetails ? state.dataSourceDetails.selectedDataSourceName : '');
const dataSourceSelector = (state) => (state.dataSourceDetails ? state.dataSourceDetails.dataSourceDetails : {});
const previewDataSelector = (state) => (state.dataSourceDetails ? state.dataSourceDetails.previewData : []);

const makeSelectedDataSource = () => createSelector(
    selectedDataSourceSelector,
    (selectedDataSourceId) => selectedDataSourceId || 0
);

const getDataSourceDetails = () => createSelector(
    dataSourceSelector,
    (dataSourceDetails) => {
        return { ...dataSourceDetails };
    }
);
const getPreviewData = () => createSelector(
    previewDataSelector,
    (previewData) => previewData.map((preview) => ({ ...preview }))
);

export {
    makeSelectedDataSource,
    getDataSourceDetails,
    getPreviewData
};
