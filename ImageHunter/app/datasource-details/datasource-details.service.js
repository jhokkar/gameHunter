import { axiosPost } from '../utils/axios-api';

export function getDataSourceDetailsBasedOnName(dataSourceName) {
    const data = {
        data: {
            DataSourceName: dataSourceName,
        }
    };
    return axiosPost('data-source-details', data);
}

export function getDataSourceDetailsPreviewDataByName(dataSourceName) {
    const data = {
        data: [{
            "DataSource": dataSourceName,
            "RelatedVariables":[],
            "Variables":[]
        }]
    };
    return axiosPost('preview-data', data);
}
