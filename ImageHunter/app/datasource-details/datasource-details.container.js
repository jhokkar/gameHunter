import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import DatasourceDetailsComponent from './datasource-details.component';
import { getDataSourceDetails,getPreviewData} from './datasource-details.selector';
import injectReducer from 'utils/injectReducer';
import reducer from './datasource-details.reducers';
import { loadDataSourceDetails,loadPreviewData } from './datasource-details.actions';

const mapDispatchToProps = (dispatch) => ({
    loadDataSourceDetails: () => dispatch(loadDataSourceDetails()),
    loadPreviewData: (dataSourceName) => dispatch(loadPreviewData(dataSourceName))
});

const mapStateToProps = createStructuredSelector({
    dataSourceDetails: getDataSourceDetails(),
    previewData: getPreviewData()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'dataSourceDetails', reducer });

const DatasourceDetailsContainer = compose(withReducer, withConnect)(DatasourceDetailsComponent);
export default DatasourceDetailsContainer;
