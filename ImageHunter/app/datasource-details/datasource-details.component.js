/*
 * Data Source Details Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import ReactDrawer from 'react-drawer';
import CustomBadge from 'components/custom-badge';
import './datasource-details.scss';
import PreviewDataComponent from './components/preview-data.component';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class DatasourceDetailsComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      isViewDataOpen: false
    };
  }
  componentDidMount() {
    this.props.loadDataSourceDetails();
  }
  render() {
    const { dataSourceDetails } = this.props;
    console.log('dataSourceDetails', dataSourceDetails, this.props);

    return (
      <section>
        <Helmet>
          <title>DataSource Details | SwitchBoardX</title>
          <meta name="description" content="DataSource Details, SwitchBoardX" />
        </Helmet>
        <div className="datasource-details-page">
          <div className="row shadow-sm mb-4">
            <div className="px-2 d-inline-flex">
              <div >
                <Link to="/dashboard">
                  <i className="fa fa-2x half-opacity fa-chevron-circle-left"></i>
                </Link>
              </div>
              <div className="py-1 ml-1">
                <h5>DataSource Details</h5>
              </div>
            </div>
          </div>
          <div className="row datasource-overview-details">
            <div className="col-sm-4 col-md-2 col-12">
              <div className="datasource-icon shadow py-1">
                {(dataSourceDetails.ProviderLogo ?
                  <img src={dataSourceDetails.ProviderLogo} alt="data-source-logo" />
                  :
                  <i className="fa text-theme-color fa-custom fa-file"></i>
                )
                }
              </div>
            </div>
            <div className="datasource-provider-name-overview  col-sm-12 col-md-10 ">
              <div className="row no-gutters">
                <div className="col-md-4">
                  <h4 className="font-weight-bold mt-1">
                    {dataSourceDetails.DataSourceName || 'No Data Found'}
                  </h4>
                  <span className="text-muted">{dataSourceDetails.Category} </span>
                </div>
                <div className="offset-md-5 col-md-3">
                  <button
                    onClick={() => {
                      // triggered on "<" on left top click or on outside click
                      this.props.loadPreviewData(dataSourceDetails.DataSourceName);  
                      this.setState({ isViewDataOpen: true });
                    }}
                    className="btn btn-white shadow header-font-size font-weight-bold"
                  >
                    VIEW DATA
                  </button>
                </div>
              </div>
              <hr />
            </div>
          </div>
          <div className="row no-gutters">
            <div className="datasource-data-details p-2 d-flex-inline col-12 col-sm-9 ">
              <div className="row no-gutters" >
                <div className="col-12">
                  <span className="detail-header text-muted">
                    Description:
                </span>
                  <span className="detail-content">
                    {dataSourceDetails.ShortDescription}
                  </span>
                </div>
                <div className="col-12">
                  <span className="detail-header text-muted ">
                    Provider Information: <b>{dataSourceDetails.ProviderName}</b>
                  </span>
                  <span className="detail-content">
                    {dataSourceDetails.ProviderDescription}
                  </span>
                </div>
                {/* <div className="col-12">
                  <span className="detail-header  text-muted">
                    Collection Methodology:
                </span>
                  <span className="detail-content">
                    Profund Networks developed a platform to compile and analyze an excess of 3 billion routable IP addresses and their corresponding DNS and Network attributes on a quarterly basis. In addition to comprehensive IP mapping, analysis of 200 million Domains is also conducted in each quarter.
                </span>
                </div>
                <div className="col-12">
                  <span className="detail-header text-muted ">
                    Use our data for:
                </span>
                  <span className="detail-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                </span>
                </div>
                <div className="col-12">
                  <span className="detail-header  text-muted">
                    Use our data for:
                </span>
                  <span className="detail-content">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                </span>
                </div> */}
              </div>
            </div>
            <div className="datasource-details-sidebar p-1 col-12 col-sm-3 ">
              <div className="row ml-1 no-gutters">
                <div className="col-12">
                  <span className="detail-header   text-muted">
                    Related Variables:
                  </span>
                  <span className="detail-content">
                    {dataSourceDetails.RelatedVariables && dataSourceDetails.RelatedVariables.map((variable, index) => (
                      <CustomBadge label={variable[1]} key={variable[0]} ></CustomBadge>
                    ))
                    }
                    {!dataSourceDetails.RelatedVariables ? 'No Records Found' : ''}

                  </span>
                </div>
                {/* <div className="col-12">
                  <span className="detail-header  text-muted">
                    Data Types:
                  </span>
                  <div className="datatypes-detail-item">
                    <span className="detail-content">
                     
                      <span className="data-types-details">
                        <span className="data-types-details-header" >Network Growth -</span>
                        <span className="data-types-details-value"> Target Companies growing their online network at specific rates. </span>
                      </span>
                    </span>
                  </div>
                  <div className="datatypes-detail-item">
                    <span className="detail-content">
                     
                      <span className="data-types-details">
                        <span className="data-types-details-header" >Device Count -</span>
                        <span className="data-types-details-value"> Segment companies by # of devices on their network. </span>
                      </span>
                    </span>
                  </div>
                  <div className="datatypes-detail-item">
                    <span className="detail-content">
                  
                      <span className="data-types-details">
                        <span className="data-types-details-header" >eCommerce -</span>
                        <span className="data-types-details-value"> Target companies using specific eCommerce platforms. </span>
                      </span>
                    </span>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
          <ReactDrawer
            open={this.state.isViewDataOpen}
            position="bottom"

            onClose={() => {
              // triggered on "<" on left top click or on outside click
              this.setState({ isViewDataOpen: false });
            }}
          >
            <div className="preview-data">
              <div className="clear-both">
                <i
                  onClick={() => {
                    this.setState({ isViewDataOpen: false });
                  }}
                  className="fa cursor-pointer fa-times pull-right p-1"
                >
                </i>
              </div>
              {/* <PreviewDataComponent /> */}
              <PreviewDataComponent data={this.props.previewData} />
                  <div className="row">
                    <div className="col-12"> <p className="text-font-size px-2">Note: Top 200 records are shown</p></div>
                  </div>
            </div>
          </ReactDrawer>
        </div>
      </section>
    );
  }
}

DatasourceDetailsComponent.propTypes = {
  dataSourceDetails: PropTypes.object,
  loadDataSourceDetails: PropTypes.func,
};

export default DatasourceDetailsComponent;
