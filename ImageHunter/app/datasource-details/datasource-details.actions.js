import { getDataSourceDetailsBasedOnName, getDataSourceDetailsPreviewDataByName } from './datasource-details.service';
import { BIND_DATA_SOURCE_RESULTS, BIND_DATA_SOURCE_PREVIEW_DATA } from '../constants';
import { ResponseStatusEnum } from '../constants/enums/response-status-enum';

export function loadDataSourceDetails() {
  return (dispatch, getState) => {
    const dataSourceNameQuery = getState().route.location.search;
    if (dataSourceNameQuery) {
      const dataSourceName = dataSourceNameQuery.split('=')[1];
      dispatch(getState().spinnerStore.BeginTask());

      getDataSourceDetailsBasedOnName(decodeURI(dataSourceName)).then((response) => {
        console.log('getDataSourceDetailsBasedOnName', response);
        dispatch({
          type: BIND_DATA_SOURCE_RESULTS,
          payload: {
            selectedDataSourceName: dataSourceName,
            dataSourceDetails: response.data && response.data.length > 0 ? response.data[0] : {
              RelatedVariables: [],
            },
          }
        });
        dispatch(getState().spinnerStore.EndTask());
      }).catch((e) => {
        console.log('getDataSourceDetailsBasedOnName', e);
        dispatch(getState().spinnerStore.EndTask());
        dispatch(getState().notificationStore.notify('Failed to fetch Data source details', ResponseStatusEnum.Error));
      });
    }
  };
}

export function loadPreviewData(dataSourceName) {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());
    getDataSourceDetailsPreviewDataByName(dataSourceName).then((response) => {
      dispatch({
        type: BIND_DATA_SOURCE_PREVIEW_DATA,
        payload: {
          previewData: response.data
        }
      });
      dispatch(getState().spinnerStore.EndTask());
    }).catch((e) => {
      console.log('getDataSourceDetailsPreviewDataByName', e);
      dispatch(getState().spinnerStore.EndTask());
      dispatch(getState().notificationStore.notify('Failed to fetch Preview data', ResponseStatusEnum.Error));

    });
  };
}

