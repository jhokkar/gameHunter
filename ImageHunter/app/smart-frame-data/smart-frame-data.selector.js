/**
 * Smart Frame data selectors
 */

import { createSelector } from 'reselect';

const smartFrameDataSelector = (state) => (state.smartFrameData ? state.smartFrameData.smartFrameData : []);
const smartFrameDetailsSelector = (state) => state.smartFrameData.smartFrameDataDetails;


const getSmartFrameData = () => createSelector(
    smartFrameDataSelector,
    (smartFrameData) => {
        const items = smartFrameData ? smartFrameData.map((data) => ({ ...data })) : [];
        return items;
    }
);


const getSmartFrameDataDetails = () => createSelector(
    smartFrameDetailsSelector,
    (smartFrameDetails) => {
        return { ...smartFrameDetails };
    }
);
export {
    getSmartFrameData,
    getSmartFrameDataDetails
};
