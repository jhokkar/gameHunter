import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import SmartFrameDataComponent from './smart-frame-data.component';
import { getSmartFrameData, getSmartFrameDataDetails } from './smart-frame-data.selector';
import injectReducer from 'utils/injectReducer';
import reducer from './smart-frame-data.reducers';
import { loadSmartFrameData, onPublishSmartFrameClick } from './smart-frame-data.actions';
import { updateIsSelectedDataItemsChanged } from '../dashboard/dashboard.actions';

const mapDispatchToProps = (dispatch) => ({
    loadSmartFrameData: () => dispatch(loadSmartFrameData()),
    onPublishSmartFrameClick: () => dispatch(onPublishSmartFrameClick())
});

const mapStateToProps = createStructuredSelector({
    smartFrameData: getSmartFrameData(),
    smartFrameDataDetails: getSmartFrameDataDetails()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'smartFrameData', reducer });

const SmartFrameDataContainer = compose(withReducer, withConnect)(SmartFrameDataComponent);
export default SmartFrameDataContainer;
