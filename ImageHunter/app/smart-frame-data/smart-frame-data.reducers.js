/*
 * Datasource Details Reducer
 */
import { BIND_SMART_FRAME_DATA, BIND_PUBLISH_SMARTFRAME_POPUP_ONE } from '../constants';

const initialState = ({
    smartFrameData: [],
    smartFrameDataDetails: {
        FileSize: 0,
        FilePath: '',
        Frames: 0,
        Rows: 0
      }
});

function smartFrameDataReducer(state = initialState, action) {
    switch (action.type) {
        case BIND_SMART_FRAME_DATA: {
            return { ...state, smartFrameData: action.payload.smartFrameData };
        }
        case BIND_PUBLISH_SMARTFRAME_POPUP_ONE: {
            const data = action.payload;
            return { ...state, smartFrameDataDetails: { ...data } };
        }
        default:
            return state;
    }
}

export default smartFrameDataReducer;
