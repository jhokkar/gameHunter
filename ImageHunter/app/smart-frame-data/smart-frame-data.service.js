import { axiosPost } from '../utils/axios-api';

export function getSmartFrameData(selectedData) {
    
    console.log('getSmartFrameData', selectedData);
    const data = {
        data: selectedData
    };
    return axiosPost('preview-data', data);
}


export function getSmartFrameDetails(selectedData) {
    console.log('getSmartFrameDetails', selectedData);
    const data = {
        data:{
            "smartframeData":selectedData,
            "smartframeName":"CustomName"
        }
    };
    return axiosPost('publish-smart-frame', data);
  }