import { BIND_SMART_FRAME_DATA, BIND_PUBLISH_SMARTFRAME_POPUP_ONE } from '../constants';
import { getSmartFrameData, getSmartFrameDetails } from './smart-frame-data.service';
import * as _ from 'lodash';
import { ResponseStatusEnum } from '../constants/enums/response-status-enum';
import { updateIsSelectedDataItemsChanged } from '../dashboard/dashboard.actions';


function prepareSmartFramePostData(getState) {
  // post data format
  // [{
  //   "DataSource": "Bureau of Economic Analysis (BEA)",
  //   "RelatedVariables": ["Gross Domestic Product (GDP)", "Consumer Price Index (CPI) U6"],
  //   "Variables": ["GPDIC1"]
  // }]
  const postData = [];
  try {
    const selectedDataItems = getState().dashboard ? getState().dashboard.selectedDataItems : [];
    console.log('prepareSmartFramePostData', selectedDataItems);
    if (selectedDataItems.length > 0) {
      let isAnyOfRelatedVarIsNull = false;

      const groupedItemsByDataSource = _(selectedDataItems).groupBy((x) => x.ID)
        .map((value, key) => ({
          ID: key,
          DataSourceName: value[0].DataSourceName,
          RelatedVariables: value
        }))
        .value();
      groupedItemsByDataSource.map((selectedDataItem) => {
        const relatedVarArray = [];
        selectedDataItem.RelatedVariables.map((x) => {
          if (x.RVName != null) {
            relatedVarArray.push(x.RVName);
          } else {
            isAnyOfRelatedVarIsNull = true;
          }
        });
        const item = {
          DataSource: selectedDataItem.DataSourceName,
        };
        if (!isAnyOfRelatedVarIsNull) {
          item.RelatedVariables = relatedVarArray;
        } else {
          item.RelatedVariables = [];
        }
        item.Variables = [];
        postData.push(item);
      });
    }
  } catch (e) {
    console.log('Prepare Post Data for Smart frame', e);
  }
  return postData;
}

export function loadSmartFrameData() {
  return (dispatch, getState) => {
    if(getState().dashboard.isSelectedDataItemsChanged){
      const postData = prepareSmartFramePostData(getState);
      if (postData.length > 0) {
        dispatch(getState().spinnerStore.BeginTask());
        getSmartFrameData(postData).then((response) => {
          console.log('getDataSourceDetailsBasedOnName', response);
          dispatch({
            type: BIND_SMART_FRAME_DATA,
            payload: {
              smartFrameData: response.data && response.data.length > 0 ? response.data : []
            }
          });
          dispatch(updateIsSelectedDataItemsChanged(false));
          dispatch(getState().spinnerStore.EndTask());
        }).catch((e) => {
          console.log('getSmartFrameData', e);
          dispatch({
            type: BIND_SMART_FRAME_DATA,
            payload: {
              smartFrameData: []
            }
          });
          dispatch(getState().spinnerStore.EndTask());
          dispatch(getState().notificationStore.notify('Smart frame data mash failed', ResponseStatusEnum.Error));
        });
      }
      else {
        dispatch({
          type: BIND_SMART_FRAME_DATA,
          payload: {
            smartFrameData: []
          }
        });
        dispatch(getState().notificationStore.notify('No data found', ResponseStatusEnum.Error));
      }
    }
  };
}

export function onPublishSmartFrameClick() {
  return (dispatch, getState) => {
    dispatch(getState().spinnerStore.BeginTask());
    const postData = prepareSmartFramePostData(getState);
    getSmartFrameDetails(postData).then((resp) => {
      console.log('getSmartFrameDetails', resp);
      dispatch({
        type: BIND_PUBLISH_SMARTFRAME_POPUP_ONE,
        payload: resp.data
      }
      );
      dispatch(getState().spinnerStore.EndTask());
    }).catch(() => {
      dispatch(getState().spinnerStore.EndTask());
      dispatch(getState().notificationStore.notify('Unable to Publish', ResponseStatusEnum.Error));

    });
  };
}

