/*
 * Smart Frame data Page
 *
 */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import './smart-frame-data.scss';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Modal } from 'reactstrap';
import PublishSmartFrameComponent from 'components/publish-smart-frame/publish-modal-popup.component';
import TableComponent from 'components/table/table';

class SmartFrameDataComponent extends PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    // Internal actions, No need of Redux Store usage
    this.state = {
      openPublishSmartFrame: false
    };
  }
  componentDidMount() {
    this.props.loadSmartFrameData();
  }

  onOpenPublishSmartFrameModal = () => {
    this.props.onPublishSmartFrameClick();
    this.setState({ openPublishSmartFrame: true });
  };

  onClosePublishSmartFrameModal = () => {
    this.setState({ openPublishSmartFrame: false });
  };

  render() {
    return (
      <section>
        <Helmet>
          <title>Recommended Smart Frame | SwitchBoardX</title>
          <meta name="description" content="Recommended Smart Frame, SwitchBoardX" />
        </Helmet>
        <div className="smart-frame-data">
          <div className="row shadow-sm  d-flex-inline justify-content-between">
            <div className="px-2 d-inline-flex">
              <div >
                <Link to="/dashboard">
                  <i className="fa fa-2x half-opacity fa-chevron-circle-left"></i>
                </Link>
              </div>
              <div className="py-1 ml-1">
                <h5>Recommended Data Frame</h5>
              </div>
            </div>

            <div className="px-2 py-1">
              <Link to="/smart-frame">
                <input
                  type="button"
                  value="EDIT SMART FRAME"
                  className="btn btn-white btn-font-size btn-responsive mx-1"
                />
              </Link>
              <input
                type="button"
                value="PUBLISH SMART FRAME"
                className="btn btn-primary btn-font-size btn-responsive"
                onClick={this.onOpenPublishSmartFrameModal}
              />
            </div>
            <Modal isOpen={this.state.openPublishSmartFrame} toggle={this.onClosePublishSmartFrameModal}>
              <PublishSmartFrameComponent toggle={this.onClosePublishSmartFrameModal} data={this.props.smartFrameDataDetails} />
            </Modal>
          </div>
          <div className="row">
            <div className="col-md-12 col-12 py-2 mt-2">
              {this.props.smartFrameData && this.props.smartFrameData.length > 0 ?
                <TableComponent tableData={this.props.smartFrameData}></TableComponent>
                :
                <div className="text-center">Recommended Data frame not found</div>
              }
            </div>
          </div>
        </div>
      </section>
    );
  }
}

SmartFrameDataComponent.propTypes = {
  smartFrameData: PropTypes.array,
  loadSmartFrameData: PropTypes.func,
  smartFrameDataDetails: PropTypes.object,
  onPublishSmartFrameClick: PropTypes.func
};

export default SmartFrameDataComponent;
