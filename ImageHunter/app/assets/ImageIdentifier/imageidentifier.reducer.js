
const initialState = ({
    user:{}
});

function homeReducer(state = initialState, action) {
    switch (action.type) {
        case "SAVE_USER_DETAILS":
            return {...state, user: action.payload.user}
        default:
            return state;
    }
}

export default homeReducer;
