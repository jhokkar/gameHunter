<div align="center">
  SwitchBoardX Web Project
</div>
<br />
<div align="center">
  Implemented using ReactJs, Redux and Webpack
</div>

Quick Start:

1. Install node js LTS Version (Long Term Supported Version) from https://nodejs.org/en/ 
2. Check whether nodejs installed properly or not by running command `node --version`
3. Navigate to this folder
4. Run command `npm install`
5. Run Web app using command `npm start`
6. App will start on the port 3002 
7. Browse the url http://localhost:3002 to see the web app
