// Important modules this config uses
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
// Load config related information
const envFile = require('node-env-file');

let appSettingsFile = '';
console.log('Environment', process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  appSettingsFile = path.resolve(process.cwd(), 'settings/production.env');
} else if (process.env.NODE_ENV === 'demo') {
  appSettingsFile = path.resolve(process.cwd(), 'settings/demo.env');
}

try {
  envFile(appSettingsFile);
} catch (error) {
  console.log('Failed to read env file!: ', __dirname, appSettingsFile);
}
module.exports = require('./webpack.common.config')({
  mode: 'production',
  // In production, we skip all hot-reloading stuff
  entry: [
    path.join(process.cwd(), 'app/app.js')
  ],

  // Utilize long-term caching by adding content hashes (not compilation hashes) to compiled assets
  output: {
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].chunk.js'
  },

  plugins: [

    // Minify and optimize the index.html
    new HtmlWebpackPlugin({
      template: 'app/index.html',
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
      inject: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        API_SERVER_URL: JSON.stringify(process.env.API_SERVER_URL)
      },
    }),
  ],

  performance: {
    assetFilter: (assetFilename) => !(/(\.map$)|(^(main\.|favicon\.))/.test(assetFilename)),
  },
});
